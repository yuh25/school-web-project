<?php
include('db_access_details.php');
include('action_logging.php');
include("session.php");

$Recip = strval($_POST['Recip']);
$Subject = strval($_POST['Subject']);
$Body = strval($_POST['Body']);

$sqlgetID = "SELECT `usr_id`, `usr_type_guardian`, `usr_username`, 'usr_email' FROM `users` WHERE `usr_username` IN ( '$loggedInUserID', '$Recip' )";	

try{
	$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	
	$qry = $conn -> prepare($sqlgetID);
	$qry -> execute();
	$user = $qry -> fetch();
	$userID = $user['usr_id'];
	$user_type = $user['usr_type_guardian'];
	$username = $user['usr_username'];
	$userEmail = $user['usr_email'];

	$qry -> fetch();
	$userID2 = $user['usr_id'];
	$user_type2 = $user['usr_type_guardian'];
	$username2 = $user['usr_username'];
	$userEmail2 = $user['usr_email'];

	if( (intval($user_type) == 1) && (intval($user_type2) == 1) ) {
		echo "<h1>You cannot send a message to another Guardian</h1>";
	} else {
		if( $username != $Recip ){
			$temp = $username;
			$username = $username2;
			$username2 = $temp;
			$userEmail = $userEmail2;
		}
		$sql =  "INSERT INTO message (msg_sender_id, msg_recip_id, msg_subject, msg_body) values ('$loggedInUserID', '$userID', '$Subject', '$Body' )";
		$conn -> exec($sql);
		echo "<h1>Message was sent to '$Recip'</h1>";
	}
		// Write the action to the log file:		
	if($userEmail != NULL || $userEmail != ''){
		mail($userEmail,$Subject,$Body);
		logThis($loggedInUserID . " created an email for user id: " . $userID );
	} else {
		logThis($loggedInUserID . " sent a message to user id: " . $userID );
	}
} catch(PDOException $e) {

	ErrorlogThis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine() );
	echo "<h1>There was a problem sending the message</h1>";
}			
$conn = null;	
?>
