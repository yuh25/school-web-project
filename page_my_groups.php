<?php
	include("db_access_details.php");
	include("session.php");
	include('misc_functions.php');
	include('action_logging.php');				
?>
		
<HTML>
	<head>
		<title>My Groups</title>
		<link href="style.css" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<script language="javascript" type="text/javascript">
	</script>	
	</head>
	<body>
		<div id="header">
			<?php
			echo showBanner($uName, $allUTypes, $uTypeCode, "Groups");
			?>
		</div>
		<div id="leftnavigation">
			<div class='cssmenu'>
				<?php
				echo showMenu($uTypeCode);					
				?>
			</div>
		</div>
		<div id="content" name="content">
			<h1>My Groups</h1>	
			</br>
			<div id="divGroups">
				<?php	

				echo '
						<table style="width:100%; display:inline;" border="0" id="tableInbox" cellpadding="0" cellspacing="0">
							<tr style="background-color:#D0D0D0">
								<td style="width:30%">Group Name</td>
								<td style="width:50%">Members</td>		
								<td style="width:20%">Notices</td>
							</tr>
						
						';
					try {			
						$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
						$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
						$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);	
						
						$sql = "SELECT  `grp_name` 
								FROM  `users_groups` 
								JOIN  `groups` ON users_groups.grp_ID = groups.grp_id
								WHERE  `usr_id` =  '$loggedInUserID'
								AND  `usrgrp_active` =  '1'";

						$qry = $conn -> prepare($sql);
						$qry -> execute();

						//$sql = "SELECT `evt_name` FROM `event` WHERE `grp_id` = '$qry[0]'";

						$qry = $conn -> prepare($sql);
						$qry -> execute();
						
						foreach ($qry as $row){
							echo '<tr>';
							echo '<td>' . $row[0] . '</td>';
							echo '<td>0</td>';
							echo '<td>0</td>';
							echo '</tr>';
						}
						//echo $sql;

					} catch(PDOException $e) {
						Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
					}
					echo '</table>';
				?>
			</div>						
		</div>
	</body>
</html>
