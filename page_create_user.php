<?php

	/*
	 * Generates the main page for creating a user.
	 * 
	 * Simple interface to create a user record.
	 * 
	 */
	include("session.php");
	include("misc_functions.php");
	include("action_logging.php");
	include("obj_user.php");
	require'lib/password.php';

	// When the Save button is clicked, execute
	// the code in create_user.php.
	//
	if(isset($_POST['submitNewUser'])) {
		if(isset($_POST['txtNewUserUName']) && isset($_POST['txtNewUserPWord'])){
			$newUID = -1;
			$newFName = 		isset($_POST['txtNewUserFName']) ? $_POST['txtNewUserFName'] : '';
			$newLName = 		isset($_POST['txtNewUserLName']) ? $_POST['txtNewUserLName'] : '';
			$newTitle = 		isset($_POST['cboNewUserTitle']) ? $_POST['cboNewUserTitle'] : '';
			$newTypeStudent = 	isset($_POST['chkNewUserTypeStudent']) ? $_POST['chkNewUserTypeStudent'] : '';
			$newTypeAdmin = 	isset($_POST['chkNewUserTypeAdmin']) ? $_POST['chkNewUserTypeAdmin'] : '';
			$newTypeTeacher = 	isset($_POST['chkNewUserTypeTeacher']) ? $_POST['chkNewUserTypeTeacher'] : '';
			$newTypeGuardian = 	isset($_POST['chkNewUserTypeGuardian']) ? $_POST['chkNewUserTypeGuardian'] : '';
			$newEmail = 		isset($_POST['txtNewUserEmail']) ? $_POST['txtNewUserEmail'] : '';
			$newStudentYrLvl = 	isset($_POST['cboNewUserYrLevel']) ? $_POST['cboNewUserYrLevel'] : '';
			$pwHash = password_hash($_POST['txtNewUserPWord'],PASSWORD_DEFAULT);
			$newUser = new SWPUser( $newUID, $_POST['txtNewUserUName'], $pwHash, $newTitle, $newLName, $newFName, $newEmail, $newStudentYrLvl,$newTypeAdmin, $newTypeTeacher, $newTypeGuardian, $newTypeStudent, 'yes' );
			$success = $newUser -> createUser();
			if($newTypeTeacher || $newTypeGuardian){
				$success.="This is a guasrdian or Student";
			}
		} else {
			$success = "User was not created: Username or Password is missing";
		}
	}
	?>

	<HTML>
		<head>
			<title>Create User</title>
			<link href="style.css" rel="stylesheet" type="text/css" />
			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		</style>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script language="javascript" type="text/javascript">
		function showAlert() {
			alert("!");
		}

			// Hide controls that are inappropriate for a student:
			//
			function showForStudent() {
				
				if(document.getElementById("chkNewUserTypeStudent").checked == true) {

					document.getElementById("lblNewUserTypeGuardian").style.visibility = "hidden";
					document.getElementById("chkNewUserTypeGuardian").style.visibility = "hidden";
					document.getElementById("lblNewUserTypeTeacher").style.visibility = "hidden";
					document.getElementById("chkNewUserTypeTeacher").style.visibility = "hidden";
					document.getElementById("lblNewUserTypeAdmin").style.visibility = "hidden";
					document.getElementById("chkNewUserTypeAdmin").style.visibility = "hidden";
					document.getElementById("cboNewUserTitle").disabled  = true;
					//document.getElementById("lblNewUserTitle").style.visibility = "hidden";
					document.getElementById("txtNewUserEmail").disabled  = true;
					//document.getElementById("lblNewUserEmail").style.visibility = "hidden";
				} else {
					
					document.getElementById("lblNewUserTypeGuardian").style.visibility = "visible";
					document.getElementById("chkNewUserTypeGuardian").style.visibility = "visible";
					document.getElementById("lblNewUserTypeTeacher").style.visibility = "visible";
					document.getElementById("chkNewUserTypeTeacher").style.visibility = "visible";
					document.getElementById("lblNewUserTypeAdmin").style.visibility = "visible";
					document.getElementById("chkNewUserTypeAdmin").style.visibility = "visible";
					document.getElementById("cboNewUserTitle").disabled  = false;
					//document.getElementById("lblNewUserTitle").style.visibility = "visible";
					document.getElementById("txtNewUserEmail").disabled  = false;
					//document.getElementById("lblNewUserEmail").style.visibility = "visible";
				}
			}
			
			// Hide controls that are inappropriate for a non-student:
			//
			function showForNonStudent() {
				
				if(document.getElementById("chkNewUserTypeGuardian").checked == true ||
					document.getElementById("chkNewUserTypeTeacher").checked == true ||
					document.getElementById("chkNewUserTypeAdmin").checked == true) {

					document.getElementById("lblNewUserTypeStudent").style.visibility = "hidden";
				document.getElementById("chkNewUserTypeStudent").style.visibility = "hidden";
				document.getElementById("lblNewUserYrLevel").style.visibility = "hidden";
				document.getElementById("cboNewUserYrLevel").style.visibility = "hidden";
			} else {

				document.getElementById("lblNewUserTypeStudent").style.visibility = "visible";
				document.getElementById("chkNewUserTypeStudent").style.visibility = "visible";
				document.getElementById("lblNewUserYrLevel").style.visibility = "visible";
				document.getElementById("cboNewUserYrLevel").style.visibility = "visible";
			}
		}			
		</script>		
	</head>
	<body>
		<div id="header">
			<?php
			echo showBanner($uName, $allUTypes, $uTypeCode, "Create a User");
			?>
		</div>
		<div id="leftnavigation">
			<div id='cssmenu'>
				<?php
				echo showMenu($uTypeCode);				
				?>
			</div>
		</div>
		<div id="content" name="content">
			<h1>Create User</h1>
			<br>
			<div id="divNewUser" name="divNewUser" class="fieldSetNewUser">
				<form id="form-new-user" name="formNewUser" action = "" method="post">
					<p>
						<label for="chkNewUserTypeStudent" id="lblNewUserTypeStudent">Student</label>
						<input type="checkbox" id="chkNewUserTypeStudent" name="chkNewUserTypeStudent" class="" value="true" onclick="showForStudent()"/>
						
						<label for="chkNewUserTypeGuardian" id="lblNewUserTypeGuardian">&nbsp; Guardian</label>
						<input type="checkbox" id="chkNewUserTypeGuardian" name="chkNewUserTypeGuardian" class="" value="true" onclick="showForNonStudent()"/>
						
						<label for="chkNewUserTypeTeacher" id="lblNewUserTypeTeacher">&nbsp; Teacher</label>
						<input type="checkbox" id="chkNewUserTypeTeacher" name="chkNewUserTypeTeacher" class="" value="true" onclick="showForNonStudent()"/>
						
						<label for="chkNewUserTypeAdmin" id="lblNewUserTypeAdmin">&nbsp; Admin</label>
						<input type="checkbox" id="chkNewUserTypeAdmin" name="chkNewUserTypeAdmin" class="" value="true" onclick="showForNonStudent()"/>
					</p>
					<p>
						<!-- Title: -->
						<label for="cboNewUserTitle" id="lblNewUserTitle">Title</label><br>
						<select id="cboNewUserTitle" name="cboNewUserTitle" class="">
							<option value="Mrs">Mrs</option>
							<option value="Ms">Ms</option>
							<option value="Mr">Mr</option>
						</select><br>
						
						First Name<br>
						<input type="text" id="text-new-user-fname" name="txtNewUserFName" class=""><br>
						Last Name<br>
						<input type="text" id="text-new-user-lname" name="txtNewUserLName" class="">					
					</p>
					<p>
						<!-- Email: -->
						<label for="txtNewUserEmail" id="lblNewUserEmail">Email Address</label><br>
						<input type="email" id="txtNewUserEmail" name="txtNewUserEmail" class="" />
					</p>
					<p>
						<!-- Student Year Level: -->
						<label for="cboNewUserYrLevel" id="lblNewUserYrLevel">Student Year Level</label><br>
						<select id="cboNewUserYrLevel" name="cboNewUserYrLevel" class="">
							<option value=""></option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>
					</p>
					<p>
						Username<br>
						<input type="text" style="display:none;">
						<input type="text" id="text-new-user-uname" name="txtNewUserUName" class="" autocomplete='off'>
						
					</p>
					<p>
						Password<br>
						<input type="text" style="display:none;">
						<input type="password" id="text-new-user-pword" name="txtNewUserPWord" class="" autocomplete='off'>
					</p>

					<p>
						<input type="submit" id="submitNewUser" name="submitNewUser" value="Create">
						<input type="reset" id="resetNewUser" name="resetNewUser" value="Clear">
					</p>
				</form>

				<p>
					<?php
					// Shows the result of process:
					//
					if(isset($success))
						echo $success;

					// Log the attempt:
					//					
					$candidateUName = isset($_POST['txtNewUserUName']) ? $_POST['txtNewUserUName'] : '';
					
					if ($candidateUName !== '') {
						if ($success === "<h1>User created successfully</h1>") {
							
							logThis($uName . " created user: " . $candidateUName);
						}
						if ($success === "<h1>User NOT created!</h1>") {

							logThis($uName . " failed to create user: " . $candidateUName);
						}
					}
					?>
				</p>
			</div>
		</div>
		<div id="footer">
			<h2>Bottom</h2>
			footer.
		</div>
	</body>
	</html>
