<?php

	/*
	 * Generates the for creating a Group.
	 */
	include("session.php");
	include("misc_functions.php");
	include("obj_user.php");
	include("func_create_group.php");
	include("action_logging.php");

	// When the Save button is clicked, execute
	// the code in create_user.php.
	//
	if(isset($_POST['submitNewGroup'])) {
		// May need to use FILTER_SANITIZER_* and filter_var() function.
		// eg - http://code.tutsplus.com/tutorials/sanitize-and-validate-data-with-php-filters--net-2595

		$newGroupName = isset($_POST['txtgroupname']) ? $_POST['txtgroupname'] : '';
		$newGroupOwner1 = isset($_POST['txtgroupowner1']) ? $_POST['txtgroupowner1'] : '';
		$newGroupOwner2 = isset($_POST['txtgroupowner2']) ? $_POST['txtgroupowner2'] : '';
		$newGroupOwner3 = isset($_POST['txtgroupowner3']) ? $_POST['txtgroupowner3'] : '';

		$success = createGroup($newGroupName,$newGroupOwner1,$newGroupOwner2,$newGroupOwner3,$loggedInUserID);
	}
?>

<HTML>
	<head>
		<title>Create Group</title>
		<link href="style.css" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<script type="text/javascript">

		function showAlert() {
			alert("!");
		}

		var ownerCount = 2;
		var boxName = 0;
		function addOwner(){	
			if(ownerCount < 4){	
				var form = document.getElementById('formfield');			
				var text = document.createElement('input');				
				text.type = "text";
				text.id = "text-new-groupOwner";
				text.name="txtgroupowner"+ownerCount;
				var p = document.createTextNode("Group Owner Username " + ownerCount);
				form.appendChild(p);
				form.appendChild(document.createElement('br'));
				form.appendChild(text);
				form.appendChild(document.createElement('br'));				
				if(ownerCount > 2){
					document.getElementById('addOwnerBtn').style.visibility = "hidden";
				}
				ownerCount += 1;
			}			
		}

		</script>		
	</head>
	<body>
		<div id="header">
			<?php
			echo showBanner($uName, $allUTypes, $uTypeCode, "Banner Header Here");
			?>
		</div>
		<div id="leftnavigation">
			<div class='cssmenu'>
				<?php
				echo showMenu($uTypeCode);									
				?>
			</div>
		</div>
		<div id="content" name="content">
			<h1>Create Group</h1><br>			
			<div id="divForm" name="divForm" class="fieldSetNewUser">
				<form id="form-new-group" name="formNewUser" action = "" method="post">
					<fieldset >
						<p id="formfield">
							Group Name<br>
							<input type="text" id="text-new-groupName" name="txtgroupname" class="" ><br>
							Group Owner Username<br>
							<input type="text" id="text-new-groupOwner" name="txtgroupowner1" class=""><br>
						</p>
					</fieldset>
					<p>
						<input type="submit" id="submitNewGroup" name="submitNewGroup" value="Create Group">
						<input type="reset" id="resetNewGroup" name="resetNewGroup" value="Clear">
						<input type="button" id="addOwnerBtn" onclick="addOwner()" value="Additional Owner">
					</p>
				</form>			
				<p>
					<?php
					// Shows the result of process:					
					if(isset($success))
						echo $success;

					// Log the attempt:			
					$candidateUName = isset($_POST['txtgroupname']) ? $_POST['txtgroupname'] : '';

					if ($candidateUName !== '') {
						if ($success === "<h1>Group created successfully</h1>")
							logThis($uName . " created group: " . $candidateUName);						
						if ($success === "<h1>Group NOT created!</h1>") 
							logThis($uName . " failed to create Group: " . $candidateUName);
					}
					?>
				</p>
			</div>
		</div>
		<div id="footer">
			<h2>Bottom</h2>
			footer.
		</div>
	</body>
</html>

