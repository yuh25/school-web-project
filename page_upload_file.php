<?php

	/*
	 * Generates the main page for uploading a file.
	 */
	 
	include('session.php');
	include('misc_functions.php');
	include('upload_file.php');
	
	if(isset($_POST['submitFile'])) {
 		
		$successfulUpload = FALSE;
		$successfulUpload = uploadFile($loggedInUserID); // Pass-in Usr ID of logged-in user.
		//echo $successfulUpload;
	}
?>

<HTML>
<head>
<title>Upload</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
</style>
		<script language="javascript" type="text/javascript">
			
			function showAlert() {
				alert("!");
			}
		</script>		
</head>
<body>
<div id="header">
	<?php
		echo showBanner($uName, $allUTypes, $uTypeCode, "Banner Header Here");
	?>
</div>

<div id="leftnavigation">

    <div id='cssmenu'>
<?php
	if($uTypeCode > 0 && $uTypeCode < 8) {
		echo showTeacherMenu();
	}
 ?>
	</div>

</div>

<div id="content" name="content">
	<h1>Upload A File</h1>
	</br></br>
	<div id="divNewUser" name="divNewUser" class="fieldSetNewUser">
		<p>
			<!-- <form id="form-upload-file" name="frmUploadFile" action = "upload_file.php" method="post"> -->
			<form id="form-upload-file" name="frmUploadFile" action = "" method="post" enctype="multipart/form-data">
				<p>
					Click to choose file to upload: &nbsp; <input type="file" name="fileToUpload" id="fileToUpload">
				</p>
				
				<p>
					<!-- Show a drop-down box of groups to assign the file to. -->
					Select group to assign file to: &nbsp;

					<select id="cboGroupsForFile" name="cboGroupsForFile" class="">
					<?php
						echo getListOfGroups();	// Found in misc_functions.php
					?>
					</select>
				</p>

				<p>
					Click to save file: &nbsp; <input type="submit" name="submitFile" id="submitFile" value="Upload File">
				</p>
				<p>
					<button type="reset" name="cancelUpload" id="cancelUpload" value="Cancel"> Cancel </button>
				</p>
				
			</form>
				
			<?php

				if($successfulUpload == TRUE) {
			
					echo '</br></br><h2>File Successfully Uploaded.</h2>';
			
				}
			
				// Show a drop-down box of groups to assign the file to.
				// Save the location of the file, along with the group it's allocated to,
				// to the database:
				//
				
				
				// TABLE: upload (
				//		upl_id, 		<-- id for the record.
				//		upl_usr_id,		<-- user id of who uploaded it. 
				//		upl_file_path)	<-- path to the file, including filename (limit: 200 chars)
				//
				// TABLE: upload_permissions (
				//		uplperm_id,		<-- id for the record.
				//		upl_id,			<-- id for the record above.
				//		uplperm_is_from_student		<-- TRUE if the upload is by a student.
				//		uplperm_readable_by_grpid)	<-- grp_id of who the file was allocated to, to be read by.
				//
				
			?>
		</p>
	</div>
<div id="footer">
<h2>Bottom</h2>
footer.
</div>
</body>
</html>