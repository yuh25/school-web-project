<?php

	/*
	 * Sub-HTML used called by the AJAX JS function loadUsersDetails()
	 *    on the module, page_edit_user.php
	 * 
	 * After the desired user to edit has been selected, this generates the edit
	 * form showing all the data that can be changed.
	 * 
	 * When the save button is clicked, it calls back to the AJAX JS function saveEditedUser()
	 *    on the original calling module, page_edit_user.php
	 */
?>
<!DOCTYPE html>
<html>
	<head>
		<style>
			table {
			    width: 100%;
			    border-collapse: collapse;
			}
			
			table, td, th {
			    border: 1px solid black;
			    padding: 5px;
			}
			
			th {text-align: left;}
		</style>
	</head>
<body>
	<?php
    $q = $_GET['q'];

	include("db_access_details.php");
	include("action_logging.php");

	try {
			
		$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
		$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

 		$qry = $conn->prepare("select * from users where usr_username = '$q'");
		$qry->execute();
		$userData = $qry -> fetch();
		
		// Load data in variables:
		//		
		$editUsrID = $userData['usr_id'];

		if (!strlen($editUsrID) == 0){
			$editUsrName = $userData['usr_username'];
			$editUsrTypeAdmin = $userData['usr_type_admin'];
			$editUsrTypeTeacher = $userData['usr_type_teacher'];
			$editUsrTypeGuardian = $userData['usr_type_guardian'];
			$editUsrTypeStudent = $userData['usr_type_student'];
			$editUsrPassword = $userData['usr_password'];
			$editUsrTitle = $userData['usr_title'];
			$editUsrLName = $userData['usr_lname'];
			$editUsrFName = $userData['usr_fname'];
			$editUsrEmail = $userData['usr_email'];
			$editUsrStudentLevel = $userData['usr_student_yrlevel'];
			$editUsrActive = $userData['usr_active'];
			
			//$editUser = new SWPUser();		
			
			//
			// show html elements holding users data here
			//
			$strHTML = '
			<form id="formEditUser" name="formEditUser" action = "" method="post">
				<fieldset>

					<p>
						<input type="checkbox" id="chkEditUserTypeStudent" name="chkEditUserTypeStudent" class="" value="true" '; 
						if($editUsrTypeStudent) {
							$strHTML = $strHTML . 'checked';
						}
						$strHTML = $strHTML . '/>Student </br>
						<input type="checkbox" id="chkEditUserTypeGuardian" name="chkEditUserTypeGuardian" class="" value="true" ';
						if($editUsrTypeGuardian) {
							$strHTML = $strHTML . 'checked';
						}
						$strHTML = $strHTML . '/>Guardian </br>
						<input type="checkbox" id="chkEditUserTypeTeacher" name="chkEditUserTypeTeacher" class="" value="true" ';
						if($editUsrTypeTeacher) {
							$strHTML = $strHTML . 'checked';
						}
						$strHTML = $strHTML . '/>Teacher </br>
						<input type="checkbox" id="chkEditUserTypeAdmin" name="chkEditUserTypeAdmin" class="" value="true" ';
						if($editUsrTypeAdmin) {
							 $strHTML = $strHTML . 'checked';
						}
						$strHTML = $strHTML . '/>Admin </br> 
					</p>
					';
					
					$strHTML = $strHTML . '
					<p>
						Title:
						<select id="cboEditUserTitle" name="cboEditUserTitle" class="">
					';
					$strHTML = $strHTML . '
							<option value = "" ';
					if($editUsrTitle === NULL) {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '></option>';				
					$strHTML = $strHTML . '
							<option value="Mrs" ';
					if($editUsrTitle == 'Mrs') {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '>Mrs</option>
							<option value="Ms" ';
					if($editUsrTitle == 'Ms') {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '>Ms</option>
							<option value="Mr" ';
					if($editUsrTitle == 'Mr') {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '>Mr</option>
						</select>
						&nbsp;&nbsp;&nbsp;
					
						First Name:
						<input type="text" id="txtEditUserFName" name="txtEditUserFName" class=""';
					$strHTML = $strHTML . 'value="'. $editUsrFName . '" />
						Last Name:
						<input type="text" id="txtEditUserLName" name="txtEditUserLName" class=""';
					$strHTML = $strHTML . 'value="' . $editUsrLName . '" />					
					</p>
					<p>
						Email:
						<input type="text" id="txtEditUserEmail" name="txtEditUserEmail" class=""';
					$strHTML = $strHTML . 'value="' . $editUsrEmail . '" />
					</p>
					<p>
						Student Year Level:
						<select id="cboEditUserYrLevel" name="cboEditUserYrLevel" class="">
							<option value="" ';
					if($editUsrStudentLevel === NULL) {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '
							></option>
							<option value="7" ';
					if($editUsrStudentLevel == 7) {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '
							>7</option>
							<option value="8" ';
					if($editUsrStudentLevel == 8) {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '
							>8</option>
							<option value="9" ';
					if($editUsrStudentLevel == 9) {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '		
							>9</option>
							<option value="10" ';
					if($editUsrStudentLevel == 10) {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '
							>10</option>
							<option value="11" ';
					if($editUsrStudentLevel == 11) {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '
							>11</option>
							<option value="12" ';
					if($editUsrStudentLevel == 12) {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '
							>12</option>
						</select>
					</p>

					<p>
						User Active:
						<select id="cboEditUserActive" name="cboEditUserActive" class="">
							<option value="yes" ';
					if($editUsrActive == 'yes') {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '
							>Yes</option>
							<option value="no" ';
					if($editUsrActive == 'no') {
						$strHTML = $strHTML . 'selected="selected" ';
					}
					$strHTML = $strHTML . '
							>No</option>
						</select>
					</p>
					<p>
						Username:
						<input type="text" id="txtEditUserUName" name="txtEditUserUName" class="" ';
					$strHTML = $strHTML . 'value="'. $editUsrName . '" />
					</p>
					<p>
						Password:
						<input type="password" id="txtEditUserPWord" name="txtEditUserPWord" class="" ';
					$strHTML = $strHTML . 'value="" /><br>
					Confirm Password:
						<input type="password" id="txtconfirmUserPWord" name="txtconfirmUserPWord" class="" ';
					$strHTML = $strHTML . 'value="" />
					</p>					
				</fieldset>
			</form>
			';

			// Command buttons below:
			//
			$strHTML = $strHTML .'
				<p>
					<input type="submit" id="cmdSaveEditUser" name="cmdSaveEditUser" value="Save" onclick="saveEditedUser(';
			$strHTML = $strHTML . (($editUsrTypeStudent) ? 'true' : 'false') . ', ';
			$strHTML = $strHTML . (($editUsrTypeGuardian) ? 'true' : 'false') . ', ';
			$strHTML = $strHTML . (($editUsrTypeTeacher) ? 'true' : 'false') . ', ';
			$strHTML = $strHTML . (($editUsrTypeAdmin) ? 'true' : 'false') . ', ';
			$strHTML = $strHTML . '\'' . $editUsrFName . '\', ';
			$strHTML = $strHTML . '\'' . $editUsrLName . '\', ';
			$strHTML = $strHTML . '\'' . $editUsrEmail . '\', ';
			$strHTML = $strHTML . '\'' . $editUsrName . '\', ';
			$strHTML = $strHTML . '\'' . $editUsrPassword . '\', ';
			$strHTML = $strHTML . '\'' . $editUsrTitle . '\', ';
			$strHTML = $strHTML . '\'' . $editUsrStudentLevel . '\', ';
			$strHTML = $strHTML . '\'' . $editUsrActive . '\', ';
			$strHTML = $strHTML . $editUsrID . ')" />
				</p>
			</form>
			';
			
			echo $strHTML;
		} else {
			echo "<h1>Unknown Username</h1>";
		}

	} catch(PDOException $e) {
	    ErrorlogThis($e -> getMessage());
	    echo "<h1>Unknown Username</h1>";
	}
	
	$conn = null;
?>
</body>
</html>
