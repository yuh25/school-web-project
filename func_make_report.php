<?php
    
    function createSWPGroupReport($groupToReportOn, $uName) {
    	/*
		 * Creates a .CSV file on the server for the group id passed-in.
		 * 
		 * @param str	grp id to report on, or 'all' for all groups.
		 * @return bool	TRUE if successful; FALSE otherwise.
		 */
			
		$returnReportStatus = FALSE;
		
		// Report Format (CSV):
		//
		// REPORT NAME: Group Report for: [All Groups | Group Name]
		// GENERATION DATESTAMP : yyyy-mm-dd HH:mm:ss
		// User: Prepared For: logged-in user last name, logged-in user first name
		// (Blank Line)
		// (n to 1)		{
		//					Group Name + Group Owner 1 + Group Owner 2 + Group Owner 3 + 
		//					Student Last Name + Student First Name + Student Year Level
		//				} 
		//
		try{
			
			include('db_access_details.php');

			$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
			$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

			// Following SQL is adequate for when all groups is desired:
			//
			$sql = '
				select
					grp.grp_name group_name,
					usr1.usr_lname teacher_1_last_name, usr1.usr_fname teacher_1_first_name,
					usr2.usr_lname teacher_2_last_name, usr2.usr_fname teacher_2_first_name,
					usr3.usr_lname teacher_3_last_name, usr3.usr_fname teacher_3_first_name,
					student.usr_lname student_lname,
					student.usr_fname student_fname,
					student.usr_student_yrlevel student_yr_level
				from school_group grp
				inner join usr usr1
					on grp.grp_owner1 = usr1.usr_id
				left join usr usr2
					on grp.grp_owner2 = usr2.usr_id
				left join usr usr3
					on grp.grp_owner3 = usr3.usr_id
				inner join usr_in_group usrgrp
					on grp.grp_id = usrgrp.grp_id
				inner join usr student
					on usrgrp.usr_id = student.usr_id
				where 1 = 1
				and student.usr_active = 1
				and usrgrp.usrgrp_active = 1
				';
				
			if($groupToReportOn === 'all') {

				$fileNameToCreate = 'allGroups.csv';
			} else {

				// Generate a report for specified group:
				//
				
				// Need to append which group we're wanting to report on:
				//
				$sql = $sql . ' ' . ' and grp.grp_id = '. intval($groupToReportOn);

				$fileNameToCreate = 'Group.csv';
			}

			$rptData = $conn->query($sql);

			$reportHandle = fopen('reports/' . $fileNameToCreate, "w") or die('Unable to create reports.');
			fwrite($reportHandle, "Group Report For: All Groups" . "\r\n");
			fwrite($reportHandle, date('Y-m-d') . ' ' . date('H:i:s') . "\r\n");
			fwrite($reportHandle, 'Prepared for: ' . $uName . "\r\n");
			fwrite($reportHandle, "\r\n");
			$rptHeader = "Group Name, Teacher 1 Last Name, Teacher 1 First Name, ";
			$rptHeader = $rptHeader . "Teacher 2 Last Name, Teacher 2 First Name, ";
			$rptHeader = $rptHeader . "Teacher 3 Last Name, Teacher 3 First Name, ";
			$rptHeader = $rptHeader . "Student Last Name, Student First Name, Student Year Level";
			fwrite($reportHandle, $rptHeader . "\r\n");

			// Loop through data, writing it to the file:
			//
			while($row = $rptData->fetch(PDO::FETCH_ASSOC)) {
				
				$dataToWrite = "";
				$dataToWrite = $dataToWrite . trim($row['group_name']) . ",";
				$dataToWrite = $dataToWrite . trim($row['teacher_1_last_name']) . ",";
				$dataToWrite = $dataToWrite . trim($row['teacher_1_first_name']) . ",";
				$dataToWrite = $dataToWrite . trim($row['teacher_2_last_name']) . ",";
				$dataToWrite = $dataToWrite . trim($row['teacher_2_first_name']) . ",";
				$dataToWrite = $dataToWrite . trim($row['teacher_3_last_name']) . ",";
				$dataToWrite = $dataToWrite . trim($row['teacher_3_first_name']) . ",";
				$dataToWrite = $dataToWrite . trim($row['student_lname']) . ",";
				$dataToWrite = $dataToWrite . trim($row['student_fname']) . ",";
				$dataToWrite = $dataToWrite . strval($row['student_yr_level']);
				$dataToWrite = $dataToWrite . "\r\n";
				
				fwrite($reportHandle, $dataToWrite);
			}
			
			fclose($reportHandle);
			
			// Record the log in:
			//
			include("php_logging/action_logging.php");
			logThis($uName . " generated the report: User Reports, for group: " . $groupToReportOn);

			$returnReportStatus = TRUE;
		} catch(PDOException $e) {
		    echo 'ERROR: ' . $e -> getMessage();
		    //some_logging_function($e->getMessage());
		}
		
		if ($returnReportStatus) {
			zipFile('reports/' . $fileNameToCreate);
		}
		
		// should this need to sleep or loop a 
		// few times to see if the file exists...?
		//
		if ($groupToReportOn === 'all') {
			// Download the file, allGroups.zip:
			//
			if (file_exists('reports/allGroups.zip')) {
	
				header( "Pragma: public" );
				header( "Expires: 0" );
				header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
				header( "Cache-Control: public" );
				header( "Content-Description: File Transfer" );
				header( "Content-type: application/zip" );
				header( "Content-Disposition: attachment; filename=\"" . "allGroups.zip" . "\"" );
				header( "Content-Transfer-Encoding: binary" );
				header( "Content-Length: " . filesize( 'reports/allGroups.zip' ) );
				
				readfile( 'reports/allGroups.zip' );
			}
		} else {

			// Download the file, Group.zip:
			//
			if (file_exists('reports/Group.zip')) {
	
				header( "Pragma: public" );
				header( "Expires: 0" );
				header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
				header( "Cache-Control: public" );
				header( "Content-Description: File Transfer" );
				header( "Content-type: application/zip" );
				header( "Content-Disposition: attachment; filename=\"" . "Group.zip" . "\"" );
				header( "Content-Transfer-Encoding: binary" );
				header( "Content-Length: " . filesize( 'reports/Group.zip' ) );
				
				readfile( 'reports/Group.zip' );
			}
		}
		$conn = null;

		return $returnReportStatus;
    }