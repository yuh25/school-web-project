<?php

	/*
	 * Sub-HTML used called by the AJAX JS function saveEditedUser()
	 *    on the module, page_edit_user.php
	 * 
	 * When the save button has been clicked, all the new variables are passed here to be
	 * actually saved to the DB.  Also displays a string showing if the save was successful
	 * or not.
	 */
	

?>
<!DOCTYPE html>
<html>
	<head>
		<style>
			table {
			    width: 100%;
			    border-collapse: collapse;
			}
			
			table, td, th {
			    border: 1px solid black;
			    padding: 5px;
			}
			
			th {text-align: left;}
		</style>
	</head>
<body>
	<?php
		
	/*
	 * Validates + saves the edited user data to the table, [usr].
	 * 
	 * Writes responses back to the web page. 
	 * 
	 */	 

	// Used to get data for currently logged-in user (ie, the session):
	//
	include('session.php');
	include('action_logging.php');
	require 'lib/password.php';
	 	
	// Get the passed-in variables:
	//
	$userID = intval($_GET['userIDEdit']);
	$typeStudent = strval($_GET['typeStudent']);
	$typeGuardian = strval($_GET['typeGuardian']);
	$typeTeacher = strval($_GET['typeTeacher']);
	$typeAdmin = strval($_GET['typeAdmin']);
	$newTitle = strval($_GET['newTitle']);
	$newLName = strval($_GET['newLName']);
	$newFName = strval($_GET['newFName']);
	$newEmail = strval($_GET['newEmail']);
	$newYrLevel = strval($_GET['newYrLevel']);
	$newActive = strval($_GET['newActive']);
	$newUName = strval($_GET['newUName']);
	$newPWord = strval($_GET['newPWord']);
	$newConfirmPWord = strval($_GET['newConfirmPWord']);

	if($newPWord == $newConfirmPWord){
		if(!strlen($newPWord) == 0){
			$newPWord = password_hash($newPWord,PASSWORD_DEFAULT);

			// With the variables obtained, save 'em all to the DB:
			//
		    include('db_access_details.php');

			// Build-up the update statement:
			//
			$sql = 'update users ';
			$sql = $sql . 'set usr_username = \'' . $newUName . '\', ';
			$sql = $sql . 'usr_type_admin = ' . ($typeAdmin === 'true' ? 1 : 0 ) . ', ';
			$sql = $sql . 'usr_type_teacher = ' . ($typeTeacher === 'true' ? 1 : 0 ) . ', ';
			$sql = $sql . 'usr_type_guardian = ' . ($typeGuardian === 'true' ? 1 : 0 ) . ', ';
			$sql = $sql . 'usr_type_student = ' . ($typeStudent === 'true' ? 1 : 0 ) . ', ';
			$sql = $sql . 'usr_password = \'' . $newPWord . '\', ';
			$sql = $sql . 'usr_title = \'' . $newTitle . '\', ';
			$sql = $sql . 'usr_lname = \'' . $newLName . '\', ';
			$sql = $sql . 'usr_fname = \'' . $newFName . '\', ';
			$sql = $sql . 'usr_email = \'' . $newEmail . '\', ';
			if($newYrLevel)
				$sql = $sql . 'usr_student_yrlevel = ' . $newYrLevel . ', ';
			$sql = $sql . 'usr_active = ' . ($newActive === 'yes' ? 1 : 0 ) . ' ';
			$sql = $sql . 'where usr_id = ' . $userID;
			
			// For the purposes of the prototype, just save the whole record.
			// For version 1.0 however, only update the fields that have changed - this is
			// why we're passing-in the original's values, as well as reading the values off
			// the HTML interface.  It will have more control over sanitising input, further
			// validating the input, etc.
			//
			try{				
				$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
				$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			
				$conn -> exec($sql);
				
				echo '<h1> User data was Updated. </h1>';		

				logThis($uName .  " edited user: " . $newUName);
			} catch(PDOException $e) {
						
				$err = $e -> getMessage();
				echo '<p> Please contact the SWP Development Team. </p>'.$err;

				logThis($uName. " attempted to edit user: " . $newUName . " ERR: " . $err);
			}
					
			$conn = null;
		} else {
			echo '<h1>User was not updated: Password cannot be empty </h1>';
		}	
	} else{
		echo '<h1>User was not updated: Passwords are different </h1>';
	}

	?>
</body>
</html>