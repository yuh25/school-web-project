<?php
include("db_access_details.php");
include("session.php");
include('misc_functions.php');
include('action_logging.php');

$groupName = $_GET['q'];

try {			
	$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	$sql = "SELECT *
			FROM groups
			WHERE grp_ID =  '$groupName'";
	$qry = $conn -> prepare($sql);
	$qry -> execute();
	$rows = $qry->fetch();

	$sql2 = "SELECT usr_username from users_groups 
			JOIN `users`on users_groups.usr_id = users.usr_id
			where grp_id = '$groupName' AND usrgrp_active = 1";
	$qry2 = $conn -> prepare($sql2);
	$qry2 -> execute();

	echo "
	<form id='formEditGroup' name='formEditGroup' action = '' method='post'>		
		<h1 name='$groupName'>Editing Group '$rows[1]'</h1>
		Group Name:<br>
		<input id='groupName' name='groupName' type=text value=$rows[1]><br><br>
		Group Owner 1 ID:<br>
		<input name='groupOwner1' type=text value=$rows[2]><br><br>
		Group Owner 2 ID:<br>
		<input name='groupOwner2' type=text value=$rows[3]><br><br>
		Group Owner 3 ID:<br>
		<input  name='groupOwner3' type=text value=$rows[4]><br><br>
		<p>
			<input type='submit' id='editGroup' name='editGroup' value='Edit'>
			<input type='reset' id='resetEditGroup' value='Clear'>
		</p>
	</form>
	<h1>Members:</h1>";
	foreach ($qry2 as $Members){		
		echo $Members[0] . "<button onclick='removeMember(`".$Members[0]."`,`".$rows[1]."`)' style='margin-left:100px'>Remove</button><br><br>";
	}

	echo "	Add User to group:<br><input type=text id='newMember'></input>
			<button onclick='addMember()'>Add Member</button>
			<p id='result'></p>";




} catch(PDOException $e) {
	Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
}
?>

