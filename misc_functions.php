<?php
	//include("action_logging.php");
	 //Miscellaneous functions   
    /*
	 * 		Reformats the variable passed into a format
	 * 		that is SQL-like.
	 * 
	 * 		NB: Not really being used yet, could be scrapped/redone.
	 * 
	 * 		@$d - date to be reformatted
	 */
    function dbaxDateFormat($d) {
    	
		return date('Y-m-d h:i:s', strtotime(str_replace('/', '-', $d)));
    }
	
	/*
	 * Function to build a combo list of groups as defined in the table, [school_group].	 * 

	 *   2.  "list user types" implies it's using [usr].[usr_type_*], which this is NOT,
	 *       it's using the groups as defined in the table above.
	 * 
	 * @return str	The HTML for the combo box.
	 */
	function ListGroups() {
		
		include("db_access_details.php");
		try {
			
			$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
			$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		
			$q = $conn->query('select grp_id, grp_name from groups where grp_active = true');

			if(count($q->fetchAll() > 0)) {
				
				$userTypeCboList = '<select id="combo-list-user-groups" name="cboListUserGroups" class="" onchange="ShowGroupDetails(this.value)">';
				$userTypeCboList = $userTypeCboList . '<option value=""></option>';
				foreach($conn->query('select grp_id, grp_name from groups where grp_active = true') as $r) {
					$userTypeCboList = $userTypeCboList . '<option value="' . $r['grp_id'] . '">' . $r['grp_name'] . '</option>\n';
				}
				
				$userTypeCboList = $userTypeCboList . '</select>';
			}
		} catch(PDOException $e) {
		    ErrorlogThis($e -> getMessage());
		    echo "An Error Has occured, please contact an administrator";
		}
		
		$conn = null;
		
		return $userTypeCboList;
	}
	
	function getListOfGroups() {
		/*
		 * Function used to build-up the user groups to populate a combo drop-down box in html
		 * 
		 * @returns: inner html for a drop-down box (i.e., "<select> ... </select>")
		 */		
		include("db_access_details.php");
		try {
			
			$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
			$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		
			$q = $conn->query('select grp_id, grp_name from groups where grp_active = true');
			
			$userGroupsCboList = '';

			if(count($q->fetchAll() > 0)) {
				foreach($conn->query('select grp_id, grp_name from groups where grp_active = true') as $r) {
					$userGroupsCboList = $userGroupsCboList . '<option value="' . $r['grp_id'] . '">' . $r['grp_name'] . '</option>\n';
				}
			}			
		} catch(PDOException $e) {
		    ErrorlogThis($e -> getMessage());
		    echo "An Error Has occured, please contact an administrator";
		}		
		$conn = null;		
		return $userGroupsCboList;
	}

	/*
	 * Zip files function.
	 * 
	 * Zips a bunch of files in an array, to the value specified in $destination.
	 * 
	 * @param array()	List of file names to zip, each as a string.
	 * @param str	path to create the zipped files.
	 * @return bool	 TRUE if successful; FALSE otherwise.
	 * 
	 */
	function zipFileArray($filesToZip = array(), $destination) {
			
		$zip = new ZipArchive;
		$zipResult = FALSE;

		// Set flags for the $zip object's use:
		//
		if(file_exists($destination)) {
			$zipSwitch = ZIPARCHIVE::OVERWRITE;
		} else {
			$zipSwitch = ZIPARCHIVE::CREATE;
		}

		// Configure an array with the files:
		//
		$validFiles = array();
		
		foreach($filesToZip as $f) {
			if(file_exists($f)) {
				$validFiles[] = $f;
			}
		}

		// If we have files to zip:
		//
		if(count($validFiles)) {
			if($zip -> open($destination, $zipSwitch)) {
				foreach($filesToZip as $f) {
					$zip -> addFile($f, $f);
				}
				$zip -> close();
				$zipResult = TRUE;
			}	
		}
		
		return $zipResult;		
	}
	
	function zipFile($fileToZip) {
			
		$zip = new ZipArchive;
		$zipResult = FALSE;

		// Set flags for the $zip object's use:
		//
		
		$fileExt = pathinfo($fileToZip)['extension'];
		$fileName = pathinfo($fileToZip)['filename'];
		$dirName = pathinfo($fileToZip)['dirname'];
		$destination = $dirName . '/' . $fileName . '.zip';

		if(file_exists($destination)) {
			$zipSwitch = ZIPARCHIVE::OVERWRITE;
		} else {
			$zipSwitch = ZIPARCHIVE::CREATE;
		}

		// If we have files to zip:
		//
		if($zip -> open($destination, $zipSwitch)) {
			$zip -> addFile($fileToZip, $fileToZip);
			$zip -> close();
			$zipResult = TRUE;
		}	
		
		return $zipResult;
	}
	  
	function showMenu($Usertype) {	

		if($Usertype > 0 && $Usertype < 9)		
			if($Usertype == 1 ) // Guardian Menu
				return  "
					<ul>
						<li class='active'><a href='page_dashboard.php'><i class='material-icons homeIcon'>&#xE88A;</i><span>Home</span></a></li>
						<li class='active'><a href='page_messaging.php'><i class='material-icons messagesIcon'>&#xE158;</i><span>Messages</span></a></li>						
						<li class='hasChild active'><a href='page_my_groups.php'><i class='material-icons groupsIcon'>&#xE7EF;</i><span>Groups</span></a></li>
					</ul>";
			else if($Usertype == 2) // Teacher Menu
				return "
					<ul>
						<li class='active'><a href='page_dashboard.php'><i class='material-icons homeIcon'>&#xE88A;</i><span>Home</span></a></li>
						<li class='active'><a href='page_messaging.php'><i class='material-icons messagesIcon'>&#xE158;</i><span>Messages</span></a></li>						
						<li class='hasChild active'><a href='#'><i class='material-icons groupsIcon'>&#xE7EF;</i><span>Groups</span></a><ul>
								<li><a href='page_my_groups.php'>My Groups </a></li>
								<li><a href='page_create_group.php'>Create Group</a></li>
								<li><a href='page_edit_group.php'>Edit Group</a></li>
								<li><a href='page_create_event.php'>Create an Event</a></li>
								</ul>
						</li>
					</ul>";						
			else if($Usertype == 4) // Admin Menu
				return "
					<ul>
						<li class='active'><a href='page_dashboard.php'><i class='material-icons homeIcon'>&#xE88A;</i><span>Home</span></a></li>
						<li class='active'><a href='page_messaging.php'><i class='material-icons messagesIcon'>&#xE158;</i><span>Messages</span></a></li>						
						<li class='hasChild active'><a href='#'> <i class='material-icons usersIcon'>&#xE7FD;</i><span>Users</span></a><ul>
								<li><a href='page_create_user.php'>Create User</a></li>
								<li><a href='page_edit_user.php'>Edit User</a></li>
								</ul>
						</li>
						<li class='hasChild active'><a href='#'><i class='material-icons groupsIcon'>&#xE7EF;</i><span>Groups</span></a><ul>
								<li><a href='page_my_groups.php'>My Groups </a></li>
								<li><a href='page_create_group.php'>Create Group</a></li>
								<li><a href='page_edit_group.php'>Edit Group</a></li>
								<li><a href='page_create_event.php'>Create an Event</a></li>
								</ul>
						</li>
						<li class='active'><a href='page_report_all_groups.php'><i class='material-icons reportsIcon'>&#xE85D;</i><span>Reports</span></a></li>
					</ul>";
			else if($Usertype == 8) // Student Menu
				return "
					<ul>
						<li class='active'><a href='page_dashboard.php'><i class='material-icons homeIcon'>&#xE88A;</i><span>Home</span></a></li>
						<li class='active'><a href='page_messaging.php'><i class='material-icons messagesIcon'>&#xE158;</i><span>Messages</span></a></li>						
						<li class='hasChild active'><a href='page_my_groups.php'><i class='material-icons groupsIcon'>&#xE7EF;</i><span>Groups</span></a></li>
					</ul>";				


    }

	function showBanner($uName, $allUTypes, $uTypeCode, $bannerHeaderText) {
		
		$bannerHTML = "<h1 class='banner'>". $bannerHeaderText . "</h1>";
		$bannerHTML .=  "<div class='user'>" . $uName ."  <a id='logoutbtn'href=\"logout.php\">Log out</a> </div>";
		return $bannerHTML;
	}

	/*
	 * Builds an array containing all the files the user
	 * has uploaded that are still on the server.
	 *
	 * @var	int	User ID to interrogate the DB to get the files for
	 * @return	array of strings
	 */
	function getAllFilesForUser($usr_id) {
		
		$filesForUser = array();
		
		include('db_access_details.php');
		
		$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
		$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

		try {
			$sql = 'select distinct upl_file_path from upload where upl_usr_id = ' . $usr_id;
			$qry = $conn -> prepare($sql);
			$qry->execute();
			$allRows = $qry -> fetchAll();
			foreach($allRows as $row) {
				if(file_exists($row['upl_file_path'])) {

					$filesForUser[] = $row['upl_file_path'];
				}
			}
			
		} catch(PDOException $e) {
			echo 'ERROR: ' . $e -> getMessage();
			$conn -> rollback();
		}
			
		$conn = null;
		return $filesForUser;
	}
 ?>