<?php
    
    /*
     * kill any sessions + their session-related data.	 
	 * Also, point-back to the login page.
	 */
    include('session.php');
	include('action_logging.php');

	logThis("Successful logout: " . $uName);
	
	session_unset();
	session_destroy();

	header("Location:index.php");	

 ?>