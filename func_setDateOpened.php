<?php
include("db_access_details.php");
include("session.php");
include('misc_functions.php');
include('action_logging.php');

$msgID = intval($_POST['msgID']);

try {			
	$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	$sql ="UPDATE `message` SET `msg_date_opened`=now() WHERE msg_id = $msgID";
	$qry = $conn -> prepare($sql);
	$qry -> execute();

} catch(PDOException $e) {
	Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
}
//$conn = null;
?>

