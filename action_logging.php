<?php
    
	/*
	 * Function to get the IP address of a visiting client.
	 * 
	 * @return ip address of visiting browser.  
	 */
	function getUserIP() {

		switch(true){
	      case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
	      case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
	      case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
	      default : return $_SERVER['REMOTE_ADDR'];
    	}
    }

	/*
	 * Function to write an action to the log file.
	 * 
	 * Log file kept in: <site directory>/logs/log.txt
	 * 
	 * May have to include somesort of ability monitoring the size of the log file,
	 * and close/rename/open a new one for version 1.0.
	 */
    function logThis($descToLog) {

		$logFileHandle = "";
    	
		$dateStamp = date("Y-m-d") . " " . date("H:i:s");
		$clientIP = getUserIP();
		$dataToLog = $dateStamp . "\t" . $clientIP . "\t" . $descToLog;
		
		$logFileDir = "logs";
		$logFileName = "log.txt";
		$logFullPath = $logFileDir . "/" . $logFileName;
		
		if(file_exists($logFullPath)) {			
			$logFileHandle = fopen($logFullPath, "a") or die("Unable to open log(Check Folder Permissions).</br>" . basename(__DIR__) . "</br>" . $logFullPath . "</br>" . getcwd());
		} else {			
			$logFileHandle = fopen($logFullPath, "w") or die("Unable to open log(Check Folder Permissions).</br>" . basename(__DIR__) . "</br>" . $logFullPath . "</br>" . getcwd());
		}
		
		fwrite($logFileHandle, $dataToLog . "\r");
		fclose($logFileHandle);
    }
	
	function ErrorlogThis($descToLog) {

		$logFileHandle = "";
    	
		$dateStamp = date("Y-m-d") . " " . date("H:i:s");
		$clientIP = getUserIP();
		$dataToLog = $dateStamp . "\t" . $clientIP . "\t" . $descToLog;
		
		$logFileDir = "logs";
		$logFileName = "errors.txt";
		$logFullPath = $logFileDir . "/" . $logFileName;
		
		if(file_exists($logFullPath)) {			
			$logFileHandle = fopen($logFullPath, "a") or die("Unable to open log(Check Folder Permissions).</br>" . basename(__DIR__) . "</br>" . $logFullPath . "</br>" . getcwd());
		} else {			
			$logFileHandle = fopen($logFullPath, "w") or die("Unable to open log(Check Folder Permissions).</br>" . basename(__DIR__) . "</br>" . $logFullPath . "</br>" . getcwd());
		}
		
		fwrite($logFileHandle, $dataToLog . "\r");
		fclose($logFileHandle);
    }
	
