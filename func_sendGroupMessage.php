<?php
    include('db_access_details.php');
	include('action_logging.php');
	include("session.php");


	$Recip = strval($_POST['Recip']);
	$Subject = strval($_POST['Subject']);
	$Body = strval($_POST['Body']);

	$sqlgetID = "SELECT `usr_id` FROM `users` WHERE `usr_username` = '$Recip'";	

	try{
		$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
		$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

		// Gets the list of users in the group the message is being sent to.
		$sql = "SELECT usr_id
		FROM users_groups
		left JOIN groups
		ON groups.grp_ID=users_groups.grp_ID
		where grp_name = '$Recip'";
		$qry = $conn -> prepare($sql);
		$qry -> execute();
		$result = $qry ->fetchAll();
		$inGroup = FALSE;
		// goes though each user and checks if the user is logged in
		foreach ($result as $row){
			if(strval($row['usr_id'])==strval($loggedInUserID)){
				$inGroup = TRUE;
			}
		}
		if($inGroup){
			foreach ($result as $row){
				$userID = $row[0];
				$sql =  "INSERT INTO message (msg_sender_id, msg_recip_id, msg_subject, msg_body) values ('$loggedInUserID', '$userID', '$Subject', '$Body' )";
				$qry = $conn -> prepare($sql);
				$qry -> execute();			
			}
			echo "<h1>Message was sent to group '$Recip'</h1>";
		} else
			echo "<h1>Group does not exist or you are not in the group</h1>";		
	} catch(PDOException $e) {
			
		ErrorlogThis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine() );
		echo "<h1>There was a problem sending the message</h1>";
	}			
	$conn = null;	
?>
