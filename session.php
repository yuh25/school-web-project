<?php	
	//Establishes the session, and sets some useful variables.	
	
	include('obj_session.php');

	session_start();

	if(isset($_SESSION['login_user'])){
		$user_check = $_SESSION['login_user'];
		$loggedInUserID = $user_check -> getUsrID();
		$loggedInUserLName = $user_check -> getUsrLName();
		$loggedInUserFName = $user_check -> getUsrFName();
		$uName = $user_check -> getUsrUName();
		$allUTypes = $user_check -> getSwpUserTypeDesc();
		$uTypeCode = $user_check -> getSwpUserTypeCode();
	}else{
		header('Location: index.php');
  		exit;
	}
	
?>