<?php

	/*
	 * Validates the username + password, sets a session object if successful.
	 * 
	 * Logs the result of the attempted login.
	 */
	 	
	include('db_access_details.php');
	include('obj_session.php');
	include("action_logging.php");
	require 'lib/password.php';

	session_start();

	$error= "";
	$SQL = "";
	$rows = 0;
	
	$username = isset($_POST['credentialUsername']) ? $_POST['credentialUsername'] : '';
	$password = isset($_POST['credentialPassword']) ? $_POST['credentialPassword'] : '';

	/*
	 * NB: Being one of the first things coded, this uses "mysqli" functions.
	 * It should be re-written to use more modern, PDO functions, for Ver 1.0.
	 */

	// Sanitising input:
	//
	$username = stripslashes($username);
	$password = stripslashes($password);

	// Starting DB Access:
	//
	$conn = mysqli_connect($DBAx_dbhost, $DBAx_dbuname, $DBAx_dbpword, $DBAx_dbname);
	if(!$conn) {
		echo "Connection Error".mysqli_connect_error();
	}
	
	// Querying 
	//
	$SQL = "select * from users where usr_username = '$username'";
	$qryResult = mysqli_query($conn, $SQL);
	$rows = mysqli_num_rows($qryResult);	

	if($rows == 1){
		$foundRow = mysqli_fetch_assoc($qryResult);
		$swpPWord = $foundRow["usr_password"]; 
		if(password_verify($password, $swpPWord)){
			// Prepare the session object, swpSession:						
			$swpUName 		= $foundRow["usr_username"];
			$swpUID 		= $foundRow["usr_id"];
			$swpAdmin 		= $foundRow["usr_type_admin"];
			$swpTeacher 	= $foundRow["usr_type_teacher"];
			$swpGuardian 	= $foundRow["usr_type_guardian"];
			$swpStudent 	= $foundRow["usr_type_student"];
			$swpFName 		= $foundRow["usr_fname"];
			$swpLName 		= $foundRow["usr_lname"];
			$swpEmail 		= $foundRow["usr_email"];
			$swpStudentYrLevel = $foundRow["usr_student_yrlevel"];
			$swpActive 		= $foundRow["usr_active"];
			
			$currentSession = new SWPSession(
				$swpUName, $swpPWord, $swpUID, $swpAdmin, $swpTeacher, $swpGuardian, $swpStudent, $swpFName, $swpLName, $swpEmail, $swpStudentYrLevel, $swpActive
				);
			$_SESSION['login_user'] = $currentSession;
			// Record the log in:						
			logThis("Userlogin: " . $username);
			// Send user to the dashboard
			header("location: page_dashboard.php");
		} else {
			// Log the failed attempt, and show a fail message back to the user:
			logThis("Login Attempt with username: " . $username);
			$loginFailed = "Incorrect username or password";
		}
	} else {
			// Log the failed attempt, and show a fail message back to the user:
			logThis("Login Attempt with username: " . $username);
			$loginFailed = "Incorrect username or password";
	}		
 ?>