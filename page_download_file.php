<?php

	/*
	 * Generates a page for downloading files.
	 *
	 * Downloads aren't logged; may need to "gate" each link
	 * behind another PHP module which contains the logging as well
	 * as perform authenticating for downloading.  Furthermore, should
	 * also hash the filenames for a (miniscule) level of additional
	 * security.
	 */

	include('session.php');
	include('misc_functions.php');	
?>

<HTML>
<head>
<title>Download</title>
    <link href="style.css" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</style>
		<script language="javascript" type="text/javascript">
			
			function showAlert() {
				alert("!");
			}
		</script>		
</head>
<body>
<div id="header">
	<?php
		echo showBanner($uName, $allUTypes, $uTypeCode, "Banner Header Here");
	?>
</div>

<div id="leftnavigation">

    <div id='cssmenu'>
<?php
	if($uTypeCode > 0 && $uTypeCode < 8) {
		echo showTeacherMenu();
	}
 ?>
	</div>

</div>

<div id="content" name="content">
	<h1>Download A File</h1>
	</br></br>
	<p>Files you can download:</p>
	<p>
	<div id="divtable">
		<?php
			// Read from the DB, all the files the user has up-loaded
			// (for ver. 1.0, also get all files uploaded for the teacher's class).
			// Put them into an array:
			//
			$filesArray = getAllFilesForUser($loggedInUserID);
			
			// For the purposes of demonstrating the zipping of a file on the server,
			// zip EVERY FILE the user had uploaded.
			// Put them into an array for usage later.
			//
			$zippedFilenamesArray = array();
			foreach($filesArray as $fileToZip) {
				$fileExt = pathinfo($fileToZip)['extension'];
				$zippedFilenamesArray[] = str_replace('.' . $fileExt, '.zip', $fileToZip);
				zipFile($fileToZip);
			}

			// Build the HTML code to show a table, listing all the files available for download.
			// 
			echo '<table border="1">';
//			echo '<tr>';
//			echo '<td>';
			foreach($zippedFilenamesArray as $zips){
				echo '<tr>';
					echo '<td>';
						echo '<a href="' . $zips . '">' . $zips . '</a>';
					echo '</td>';
				echo '</tr>';
			}
//			echo '</td>';
//			echo '</tr>';
			echo '</table>';
		?>
	</div>
	</p>
</div>
<div id="footer">
<h2>Bottom</h2>
footer.
</div>
</body>
</html>