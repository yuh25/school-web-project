<?php

	/*
	 * Generates the main page for creating a user.
	 * 
	 * Simple interface to create a user record.
	 * 
	 */
	include("session.php");
	include("misc_functions.php");
	include("action_logging.php");
	include("db_access_details.php");

	// When the Save button is clicked, execute
	// the code in create_user.php.
	//
	if(isset($_POST['editGroup'])) {
		if(isset($_POST['groupName']) && isset($_POST['groupOwner1'])){ // Make sure the edited group still has a name and atleast one owner
			$gN = isset($_POST['groupName']); //GroupName
			$gO1 = isset($_POST['groupOwner1']);
			$gO2 = isset($_POST['groupOwner2']);
			$gO3 = isset($_POST['groupOwner3']);
			$gID = isset($_POST['groupID']);
			if($gN != "Guardian" && $gN != "Student" && $gN != "Teacher" && $gN != "Admin"){// Make sure not editing default group
				$sql = "UPDATE 'groups' SET 'grp_owner1'='$gO1','grp_owner2'='$gO2','grp_owner3'='$gO3' WHERE 'grp_ID'='$gID'";
			} else {	
				$sql = "UPDATE 'groups' SET 'grp_name'='$gN','grp_owner1'='$gO1','grp_owner2'='$gO2','grp_owner3'='$gO3' WHERE 'grp_ID'='$gID'";
			}
			try {			
				$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
				$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);				
				$qry = $conn -> prepare($sql);
				$qry -> execute();
				$success = $sql;//"<h1>Group was updated</h1>";
			} catch(PDOException $e) {
				Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
				$success = "<h1>Group was not updated</h1>";
			}		
		} else {
			$success = "<h1>Group was not edited: No group name or tried to edit default group</h1>";
		}
	}
	?>

	<HTML>
		<head>
			<title>Edit Group</title>
			<link href="style.css" rel="stylesheet" type="text/css" />
			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		</style>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script language="javascript" type="text/javascript">

			function showAlert() {
				alert("!");
			}

			function ShowGroupDetails(groupName) {
				if (window.XMLHttpRequest) {
			        // code for IE7+, Firefox, Chrome, Opera, Safari
			        xmlhttp = new XMLHttpRequest();
			    } 
			    else {
			        // code for IE6, IE5
			        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			    }
			    xmlhttp.onreadystatechange = function() {
			    	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			    		document.getElementById('groupDetails').innerHTML = xmlhttp.responseText;
			    	}
			    }
			    xmlhttp.open("GET","func_getGroupDetails.php?q="+groupName,true);
			    xmlhttp.send();
			}

			function removeMember(username,groupName){
				if (window.XMLHttpRequest){
				// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}else{ 
					// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						//if successfull do something
						if(xmlhttp.responseText)
							document.getElementById("result").innerHTML = "<h1>User was removed from the group:" + xmlhttp.responseText + "</h1>";
						else
							document.getElementById("result").innerHTML = "<h1>User was NOT removed from the group</h1>";
					}					
				}
				xmlhttp.open("POST", "func_removeUserFromGroup.php", true);       	
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send( 'username='+ username + '&groupName='+ groupName );
			}

			function addMember(){
				username = document.getElementById("newMember").value;
				groupName = document.getElementById("combo-list-user-groups").value;
				if (window.XMLHttpRequest){
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}else{ 
				// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						//if successfull do something
						document.getElementById("result").innerHTML = xmlhttp.responseText ;
					}					
				}
				xmlhttp.open("POST", "func_addUsertoGroup.php", true);       	
				xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				xmlhttp.send( 'username='+ username + '&groupName='+ groupName );
			}
		
		</script>		
	</head>
	<body>
		<div id="header">
			<?php
			echo showBanner($uName, $allUTypes, $uTypeCode, "Editing a Group");
			?>
		</div>
		<div id="leftnavigation">
			<div class='cssmenu'>
				<?php
				echo showMenu($uTypeCode);
				?>
			</div>
		</div>
		<div id="content" name="content">
			<h1>Edit Group</h1></br>			
			<p>
				<div id="content-sub" name="content-sub">
					Select a Group:
					<?php
					echo ListGroups();
					?>
				</div>
			</p>
			<p id='groupDetails'>
			</p>
			<p>
				<?php
					// Shows the result of process:
					if(isset($success))
						echo $success;
				?>
			</p>
		</div>
	</body>
	</html>
