<?php
include("db_access_details.php");
include("session.php");
include('misc_functions.php');
include('action_logging.php');

try {			
	$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	$sql ="SELECT  `msg_id` ,`msg_date_sent` , `msg_subject` ,  `msg_body` ,  `usr_username`, `msg_date_opened`"; 
	$sql .="FROM  `message`";
	$sql .="INNER JOIN users ON  `usr_id` =  `msg_sender_id` ";
	$sql .="WHERE  `msg_recip_id` = '$loggedInUserID'";
	$qry = $conn -> prepare($sql);
	$qry -> execute();
	//Sending the messages as XML to pass 
	$xml = new XMLWriter();
	$xml->openURI("php://output");
	$xml->startDocument();
	$xml->setIndent(true);

	$xml->startElement("messages");

	foreach ($qry as $row) {
		$xml->startElement("message");
			$xml->startElement("msg_id");
				$xml->writeRaw($row['msg_id']);
			$xml->endElement();
			$xml->startElement("usr_username");
				$xml->writeRaw($row['usr_username']);
			$xml->endElement();
			$xml->startElement("msg_date_sent");
				$xml->writeRaw($row['msg_date_sent']);
			$xml->endElement();
			$xml->startElement("msg_date_opened");
				$xml->writeRaw($row['msg_date_opened']);
			$xml->endElement();
			$xml->startElement("msg_subject");
				$xml->writeRaw($row['msg_subject']);
			$xml->endElement();
			$xml->startElement("msg_body");
				$xml->writeRaw($row['msg_body']);
			$xml->endElement();
		$xml->endElement();
	}
	$xml->endElement();		

	header('Content-type: text/xml');
	//Returns result to loadUserMessages() function
	echo $xml->flush();		
} catch(PDOException $e) {
	Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
}
//$conn = null;
?>

