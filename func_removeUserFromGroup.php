<?php
include("db_access_details.php");
include("session.php");
include('misc_functions.php');
include('action_logging.php');

$username = $_POST['username'];
$groupName = $_POST['groupName'];

try {			
	$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	$sqlUsername = "SELECT `usr_id` FROM `users` WHERE `usr_username` = '$username'";
	Errorlogthis( $sqlUsername);
	$sqlGroupName = "SELECT `grp_ID` FROM `groups` WHERE `grp_name` = '$groupName'";
	Errorlogthis( $sqlGroupName);

	$qry = $conn -> prepare($sqlUsername);
	$qry -> execute();
	$rowsUsername = $qry->fetch();

	$qry = $conn -> prepare($sqlGroupName);
	$qry -> execute();
	$rowsGroupName = $qry->fetch();

	$sql = "UPDATE `users_groups` SET `usrgrp_active`=0 WHERE grp_id = $rowsGroupName[0] AND usr_id = $rowsUsername[0]";
	$qry = $conn -> prepare($sql);
	$qry -> execute();
	

} catch(PDOException $e) {
	Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
}
?>

