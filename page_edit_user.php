<?php

	/*
	 * Generates the main page for editing a user.
	 * 
	 * Utilises AJAX to dynamically populate drop-downs + form data.
	 * 
	 * PHP modules used are:
	 *    page_edit_user.php -->
	 *       show_list_of_users.php -->
	 *          calls back to page_edit_user.php, for the JS function, loadUsersDetails() -->
	 *             show_user_details.php -->
	 *                calls back to page_edit_user.php, for the JS function, saveEditedUser() -->
	 *                   edit_user.php
	 */

	include("session.php");
	include("misc_functions.php");

	?>

	<HTML>
		<head>
			<title>Edit User</title>
			<link href="style.css" rel="stylesheet" type="text/css" />
			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		</style>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script language="javascript" type="text/javascript">

		function loadUsersDetails() {
		// Make sure the result is cleared:

 			var username = document.getElementById("usernameSearch").value;
 
			if (window.XMLHttpRequest) {
		        // code for IE7+, Firefox, Chrome, Opera, Safari
		    	xmlhttp = new XMLHttpRequest();
		    } else {
	            // code for IE6, IE5
	            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		    }
	        xmlhttp.onreadystatechange = function() {
	        	if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	        		document.getElementById("temp").innerHTML = xmlhttp.responseText;
	        	} else {
					document.getElementById("temp").innerHTML = "";
	        	}	  
	        }   
	        xmlhttp.open("GET", "show_user_details.php?q=" + username,true);
	        xmlhttp.send();		    
		}


		function saveEditedUser(
			origStudentType, origGuardianType, origTeacherType, origAdminType, 
			origFName, origLName, origEmail, origUName, origPWord, 
			origTitle, origYrLevel, origActive, editUserID
			) {
			
			// Get Check Box Values:
			//
			var newStudentType = document.getElementById("chkEditUserTypeStudent").checked;
			var newGuardianType = document.getElementById("chkEditUserTypeGuardian").checked;
			var newTeacherType = document.getElementById("chkEditUserTypeTeacher").checked;
			var newAdminType = document.getElementById("chkEditUserTypeAdmin").checked;

			// Get Text Box Values:
			//
			var newFName = document.getElementById("txtEditUserFName").value;
			var newLName = document.getElementById("txtEditUserLName").value;
			var newEmail = document.getElementById("txtEditUserEmail").value;
			var newUName = document.getElementById("txtEditUserUName").value;
			var newPWord =  document.getElementById("txtEditUserPWord").value;
			var newConfirmPWord =  document.getElementById("txtconfirmUserPWord").value;

			// Get Combo Box Values:
			//
			var cboUserTitle = document.getElementById("cboEditUserTitle");
			var newUserTitle = cboUserTitle.options[cboUserTitle.selectedIndex].value;

			var cboYrLevel = document.getElementById("cboEditUserYrLevel");
			var newYrLevel = cboYrLevel.options[cboYrLevel.selectedIndex].value;
			
			var cboActive = document.getElementById("cboEditUserActive");
			var newActive = cboActive.options[cboActive.selectedIndex].value;
			
			if (editUserID == "") {
				document.getElementById("placeholder_saveEditResult").innerHTML = "";
				return;
			} else { 
				if (window.XMLHttpRequest) {
		            // code for IE7+, Firefox, Chrome, Opera, Safari
		            xmlhttp = new XMLHttpRequest();
		        } else {
		            // code for IE6, IE5
		            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		        }

				xmlhttp.onreadystatechange = function() {
			        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			        	document.getElementById("result").innerHTML = xmlhttp.responseText;
			        }   
		    	}

		        // Pass all the data to edit_user.php, with the new data.
		        xmlhttp.open("GET","edit_user.php?userIDEdit=" + editUserID + "&typeStudent=" + newStudentType +
		        	"&typeGuardian=" + newGuardianType +  "&typeTeacher=" + newTeacherType + "&typeAdmin=" + newAdminType +
		        	"&newTitle=" + newUserTitle + "&newFName=" + newFName + "&newLName=" + newLName + 
		        	"&newEmail=" + newEmail + "&newYrLevel=" + newYrLevel + "&newActive=" + newActive +  
		        	"&newUName=" + newUName + "&newPWord=" + newPWord + "&newConfirmPWord=" + newConfirmPWord, true);
		        xmlhttp.send();		        
		    }
		}
	</script>		
</head>
<body>
	<div id="header">
		<?php
		echo showBanner($uName, $allUTypes, $uTypeCode, "Banner Header Here");
		?>
	</div>
	<div id="leftnavigation">
		<div class='cssmenu'>
			<?php
			echo showMenu($uTypeCode);
			?>			
		</div>
	</div>
	<div id="content" name="content">
		<h1>Edit User</h1></br>
		<div id="divInbox">
			Enter a Username:
			<input type='text' id='usernameSearch'>
			<button type='button' onclick='loadUsersDetails()'>Search</button>
		</div>
		</br>
		<p id='temp'></p>
		<p id='result'></p>
	</div>
</body>
</html>
