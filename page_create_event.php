<?php

	include("session.php");
	include("misc_functions.php");
	include("db_access_details.php");
	include("action_logging.php");

	if(isset($_POST['createEvent'])) {		
		if(isset($_POST['eventName']) && isset($_POST['eventDescription'])){
			if(isset($_POST['eventDateTime'])){
				try {
					$eventName = $_POST['eventName'];
					$eventDescription = $_POST['eventDescription'];
					$eventDateTime = $_POST['eventDateTime'];
					$groupID = isset($_POST['groupID'])? $_POST['groupID'] : NULL;
					$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
					$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
								
					$sql = "INSERT INTO `event`(`evt_creator_id`, `evt_name`, `evt_datetime`, `evt_desc`, `grp_id`) VALUES ('$loggedInUserID','$eventName','$eventDateTime','$eventDescription', '$groupID')";
					$conn -> exec($sql);
					$success = "<h1>The Event was successfully created</h1>";
				} catch(PDOException $e) {
					ErrorlogThis($e -> getMessage());
					$success = "<h1>There was an unknown problem creating the event</h1>";
				}				
			} else {
				$success = "<h1>Event Date and Time is missing</h1>";
			}
		} else
			$success = "<h1>Event Name or Event Description is Missing</h1>";
		$conn = null;
	}

	function getMyGroups($userID)
	{
		include("db_access_details.php");

		try {			
			$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
			$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);	
			
			$sql = "SELECT  `grp_name`, users_groups.grp_ID 
					FROM  `users_groups` 
					JOIN  `groups` ON users_groups.grp_ID = groups.grp_id
					WHERE  `usr_id` =  '$userID'
					AND  `usrgrp_active` =  '1'";

			$qry = $conn -> prepare($sql);
			$qry -> execute();
			$result = "<select name='groupID'>";
			$result .= '<option value="NULL">Public Announcement</option>';
			foreach ($qry as $row){
				$result .= '<option value="' . $row['grp_ID'] . '">' . $row['grp_name'] . '</option>';
			}
			$result .= "/<select>";
			echo $result;

		} catch(PDOException $e) {
			Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
		}
	}

?>
<HTML>
	<head>
		<title>Create an Event</title>
		<link href="style.css" rel="stylesheet" type="text/css"/>
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<script type="text/javascript">
			function showAlert(){
				alert("!");
			}
		</script>		
	</head>
	<body>
		<div id="header">
			<?php
			echo showBanner($uName, $allUTypes, $uTypeCode, "Banner Header Here");
			?>
		</div>
		<div id="leftnavigation">
			<div class='cssmenu'>
				<?php
				echo showMenu($uTypeCode);					
				?>
			</div>
		</div>
		<div id="content" name="content">
			<h1>Create an Event</h1>	
			</br>
			<form id="createEventForm" name="createEventForm" action = "" method="post">
				Event Name:<br>
				<input id='eventName' name='eventName' type='text'><br>
				Event Date and Time:<br>
				<input id='eventDateTime' name='eventDateTime' type='datetime-local'><br>
				Event Description:<br>
				<textarea name='eventDescription' id='eventDescription' rows="4" cols="50"></textarea><br>
				Event Group:<br>
				<?php 
					getMyGroups($loggedInUserID); 
				?><br><br>
				<input id='createEvent' name='createEvent' type='submit' value='Create Event'>
				<?php
					if(isset($success))
						echo $success;
				?>
			</form>
		</div>
	</body>
</html>