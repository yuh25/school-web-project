<?php
	
    /*
	 * Desc:	Class holding details for a user, for the purposes of
	 * 			creating or editing a user in the SWP.
	 */
    class SWPUser {
    	
		private $usrID = -1;				// User ID, int
		private $usrUName = "";				// Username, 45 chars
		private $usrPWord = "";				// Password, 45 chars
		private $usrTitle = "";				// Title, 3 chars
		private $usrLName = "";				// Last name, 45 chars
		private $usrFName = "";				// First name, 45 chars
		private $usrEmail = "";				// Email address, 120 chars
		private $usrStudentYrLevel = -1;	// Student year level, int
		private $usrTypeAdmin = FALSE;		// Admin user type, boolean
		private $usrTypeTeacher = FALSE;	// Teacher user type, boolean
		private $usrTypeGuardian = FALSE;	// Guardian user type, boolean
		private $usrTypeStudent = FALSE;	// Student user type, boolean
		private $usrActive = FALSE;			// Status of the record, boolean
		
		// PHP doesn't support method overloading, so allow for optional args:
		//
		function __construct($uID, $uUName, $uPWord, $uTitle, $uLName, $uFName, $uEmail, $uStudentYrLevel,$uTypeAdmin, $uTypeTeacher, $uTypeGuardian, $uTypeStudent, $uActive) {
			// Creating a new user, we don't know what the value for usr_ID / uID is yet:
			//
			if($uID === -1) {
				
				$this -> usrUName = $uUName;
				$this -> usrPWord = $uPWord;
				$this -> usrTitle = $uTitle;
				$this -> usrLName = $uLName;
				$this -> usrFName = $uFName;
				$this -> usrEmail = $uEmail;
				$this -> usrStudentYrLevel = $uStudentYrLevel;
				$this -> usrTypeAdmin = $uTypeAdmin;
				$this -> usrTypeTeacher = $uTypeTeacher;
				$this -> usrTypeGuardian = $uTypeGuardian;
				$this -> usrTypeStudent = $uTypeStudent;
				$this -> usrActive = $uActive;
			}
		}

		// Method to create the user held in this object.
		function createUser() {
			// forget it for now: check out:
			// http://code.tutsplus.com/tutorials/php-database-access-are-you-doing-it-correctly--net-25338
			
			$stmt = "";
			$success = "";
			
			include("db_access_details.php");
			
			try {				
				$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
				$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				
				$username = $this -> usrUName;
				$password = $this -> usrPWord;
				$typeAdmin = $this -> usrTypeAdmin; 
				$typeTeacher = $this -> usrTypeTeacher;
				$typeGuardian = $this -> usrTypeGuardian;
				$typeStudent = $this -> usrTypeStudent;
				$title = $this -> usrTitle;
				$lname = $this -> usrLName;
				$fname = $this -> usrFName;
				$email = $this -> usrEmail;
				$studentYrLevel = $this -> usrStudentYrLevel;
				$active = $this ->  usrActive;
				
				$stmt = "insert into users (usr_username, usr_password, usr_type_admin, usr_type_teacher, usr_type_guardian, usr_type_student, ";
				
				if($title !== "") 
					$stmt .= "usr_title, ";
				
				$stmt .= "usr_lname, usr_fname, ";

				if($email !== "") 
					$stmt .= "usr_email, ";
				
				if($studentYrLevel !== -1) 
					$stmt .= "usr_student_yrlevel";
				
				$stmt .= ") values ( '$username', '$password', ";
				
				$stmt .= ($typeAdmin != null ? '1, ':'0, ');
				$stmt .= ($typeTeacher != null ? '1, ':'0, ');
				$stmt .= ($typeGuardian != null ? '1, ':'0, ');
				$stmt .= ($typeStudent != null ? '1, ':'0, ');
				
				if($title !== "") 
					$stmt .= "'$title', ";
				
				$stmt = $stmt . "'$lname', '$fname', ";
				if($email !== "") 
					$stmt .= "'$email', ";
				
				if($studentYrLevel != null) 
					$stmt .= "'$studentYrLevel') ";
				 else 
					$stmt .= "null)";

				// Insert the newly-created user:
				
				$conn -> exec($stmt);				
				$this -> AddtoDefaultGroup($conn, $username);
				if($typeStudent){
					$success = "<h1 id='successStudent'>User created successfully</h1>";
				} else if($typeGuardian){
					$success = "<h1 id='successGuardian'>User created successfully</h1>";
				} else 
					$success = "<h1>User created successfully</h1>";
			}
			catch(PDOException $e) {
				ErrorlogThis($e -> getMessage());
				if (strpos($e -> getMessage(),'Duplicate') !== false) {
					$success = "<h1>User not created: User already Exists</h1>";
				} else if (strpos($e -> getMessage(),'Syntax error') !== false){
					$success = "<h1>User not created: One or more fields contain quotation marks</h1>";
				} else {
					$success = "<h1>User not created: Unknown Error</h1>";
				}
				
			}			
			$conn = null;			
			return $success;
		}

		function AddtoDefaultGroup($conn, $username){			
			try {
				//Converts usernames into user ID's
				$stmt = "select usr_id , usr_type_admin, usr_type_teacher, usr_type_guardian, usr_type_student from users where usr_username = '$username'";

				$temp = $conn -> query($stmt);
				//Get the ID from the result
				$row = $temp -> fetch();
				$user_id = $row["usr_id"];
				$TypeAdmin = intval($row["usr_type_admin"]);
				$TypeTeacher = intval($row["usr_type_teacher"]);
				$TypeGuardian = intval($row["usr_type_guardian"]);
				$TypeStudent = intval($row["usr_type_student"]);

				if($TypeStudent){
					$this -> InsertIntoGroup('Student', $conn, $user_id);
				} else {
					if($TypeTeacher){
						$this -> InsertIntoGroup('Teacher', $conn, $user_id);		
					}
					if($TypeAdmin){
						$this -> InsertIntoGroup('Admin', $conn, $user_id);				
					}
					if($TypeGuardian){
						$this -> InsertIntoGroup('Guardian', $conn, $user_id);
					}
				}


			} catch(PDOException $e) {
				ErrorlogThis($e -> getMessage().' on line '.$e->getLine());
			}		
		}

		function InsertIntoGroup($group_name, $conn, $userid){
			try {
				logThis('This is running');
				$stmt = "select grp_ID from groups where grp_name = '$group_name'";
				$temp = $conn -> query($stmt);
				//Get the ID from the result
				$row = $temp -> fetch();
				$group_id = $row["grp_ID"];
				$stmt = "INSERT INTO users_groups(`grp_id`, `usr_id`) VALUES ('$group_id','$userid')";
				// Insert the new group:
				$conn -> exec($stmt);
				logThis($stmt);
			}
			catch(PDOException $e) {
				ErrorlogThis($e -> getMessage() .' on line '.$e->getLine());
			}	
		}
    }
?>