<?php
    
    /*
	 * Functions for emailing for the School Web Project.
	 * 
	 * NB: hard-coded to send the email to the test email address at:
	 * 
	 *     swp_test_email@dropbearantix.com
	 * 
	 * 
	 * @var str	Email to send to
	 * @var str	Subject line to include
	 */
	function swpEmailMessage($emailAddr, $subject) {
		
		$headers = "From: swp_portal@example.com";
		
		// The following line of code is used only for the prototype.
		// Make sure it's removed for Ver 1.0.
		//
		$emailAddr = "swp_test_email@dropbearantix.com";
		
		$body = "Hello.
A message awaits for you on the School Web Portal at: 

   http://www.dropbearantix.com/prototype/
		
regards,
   Imaginary High School.";
		
		mail($emailAddr, "School Web Portal: " . $subject, $body, $headers);
	}
