<?php
include("db_access_details.php");
include("session.php");
include('misc_functions.php');
include('action_logging.php');

$username = $_POST['username'];

try {			
	$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	$sql = "SELECT usr_id
			FROM users
			WHERE usr_username =  '$username'";
	$qry = $conn -> prepare($sql);
	$qry -> execute();
	$rows = $qry->fetch(PDO::FETCH_NUM);
	echo $rows[0];	

} catch(PDOException $e) {
	Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
}
?>

