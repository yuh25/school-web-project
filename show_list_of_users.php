<?php

	/*
	 * Sub-HTML used called by the AJAX JS function showUsersList()
	 *    on the module, page_edit_user.php
	 * 
	 * After a group of users has been selected, this generates another
	 * combo box of the users in that group to be modified.
	 * 
	 * Once a user has been selected, it calls back to the AJAX JS function loadUsersDetails()
	 *    on the original calling module, page_edit_user.php
	 */
?>

<!DOCTYPE html>
<html>
	<head>
		<style>
			table {
			    width: 100%;
			    border-collapse: collapse;
			}
			
			table, td, th {
			    border: 1px solid black;
			    padding: 5px;
			}
			
			th {text-align: left;}
		</style>
	</head>
<body>
	<?php

	// Get the variable passed in:
	//
    $q = intval($_GET['q']);

	include("db_access_details.php");

	try {
			
		$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
		$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

		$sql = "select users.usr_id, users.usr_lname, users.usr_fname from users inner join users_groups where users.usr_id = users_groups.usr_id and users_groups.grp_id = ". $q;
		$sql .= " order by users.usr_lname, users.usr_fname"; 
		$qry = $conn->query($sql);
		
		// Generate the HTML for the select users' combo box:
		//
		// Each entry for the combo box is made up of: usr_id for the value, and 
		//    last name, first name for the visible text entry.
		//
		$usersList = "!";
		if(count($qry->fetchAll() > 0)) {
			$usersList = 'Select User: &nbsp;';
			$usersList .= '<select id="combo-users-to-edit" name="cboUserToEdit" class="" onchange="loadUsersDetails(this.value)">';
			$usersList .= '<option value=""></option>';
			foreach($conn->query($sql) as $r) {
				$usersList .= '<option value="' . $r['usr_id'] . '">' . $r['usr_lname'] . ', ' . $r['usr_fname'] . '</option>\n';
			}
			$usersList .= '</select>';
		}
		
		echo $usersList;

	} catch(PDOException $e) {
	    echo 'ERROR: ' . $e -> getMessage();
	}
		
	$conn = null;
			
?>
</body>
</html>
