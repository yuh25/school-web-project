<?php
	include("session.php");
	include("misc_functions.php");
?>
<HTML>
	<head>
		<title>Compose Message</title>
		<link href="style.css" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</style>
	<script language="javascript" type="text/javascript">
	
		function showAlert() {
			alert("!");
		}

		function clearAlert(){
			if (confirm('Are you sure you want to clear your message?')) { 
		    document.getElementById("rowSubjectNew").innerHTML = "Subject";
		    document.getElementById("rowRecipNew").innerHTML = "To";
		    document.getElementById("rowBodyNew").innerHTML = "Body";
			}
		}

		function loadUserMessages(){
			if (window.XMLHttpRequest){
			// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}else{ 
			// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					//if successfull do something							
					if (window.DOMParser){
						parser=new DOMParser();
						xmlDoc=parser.parseFromString(xmlhttp.responseText,"text/xml");
					} else { // Internet Explorer					
						xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
						xmlDoc.async=false;
						xmlDoc.loadXML(xmlhttp.responseText);
						document.getElementById("sendMsgResult").innerHTML = new XMLSerializer().serializeToString(xmlDoc.documentElement);
					}
					msg_subject = xmlDoc.getElementsByTagName("msg_subject");
					usr_username = xmlDoc.getElementsByTagName("usr_username");
					date_sent = xmlDoc.getElementsByTagName("msg_date_sent");
					msg_body = xmlDoc.getElementsByTagName("msg_body");
					msg_id = xmlDoc.getElementsByTagName("msg_id");
					msg_date_opened = xmlDoc.getElementsByTagName("msg_date_opened");
					

					if(msg_id[0]){
						var currentSender = usr_username[0].innerHTML;
						InsertData( msg_subject[0].innerHTML, currentSender, date_sent[0].innerHTML, msg_body[0].innerHTML, msg_id[0], msg_date_opened[0], true)
						for(i = 1; i < msg_id.length; i++){
							if( usr_username[i].innerHTML ){
								currentSender = usr_username[i].innerHTML;		
							}						
							InsertData(msg_subject[i].innerHTML, currentSender, date_sent[i].innerHTML, msg_body[i].innerHTML, msg_id[i], msg_date_opened[i], true);
						}
					} else {
						InsertData("No Messages Available", "", "", "", "", "", false); 
					}
				}
			}
	       	xmlhttp.open("POST","func_Get_User_Msgs.php",true);
	       	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	        xmlhttp.send();	        
		}

		//Creates a new row at the end of the table
		function InsertData(Subject,From,Time,body,msgID,msgDateOpened,clickable) {
			var table = document.getElementById("tableInbox");
			var row = table.insertRow(1);
			var cellSelect = row.insertCell(0);
			row.insertCell(1).innerHTML = Subject;
			row.insertCell(2).innerHTML = From;
			row.insertCell(3).innerHTML = Time;
			if(msgDateOpened.firstChild)
				row.id = msgDateOpened.firstChild.data;
			//Hide Table to display message
			if(clickable){
				cellSelect.innerHTML = "<input type='checkbox' id='" + msgID.firstChild.data + "' name='checkBox' onclick='toggleBckGnd(this)'>";
				row.cells[1].onclick = function(){ showMessage(Subject, body, From, msgID.firstChild.data) };
				row.cells[2].onclick = function(){ showMessage(Subject, body, From, msgID.firstChild.data) };
				row.cells[3].onclick = function(){ showMessage(Subject, body, From, msgID.firstChild.data) };
			}			
		}
		
		//Hides the inbox table and shows the Message Table
		function showMessage(msgSubject,msgBody,sender,msgID){
			document.getElementById("tableDisplayMsg").style.display = "inline";
			document.getElementById("divInbox").style.display = "none";
			document.getElementById("rowSubject").innerHTML = msgSubject;
			document.getElementById("rowBody").innerHTML = msgBody;
			document.getElementById("rowSender").innerHTML = "From: " + sender;
			// msg_date_opened = todays datetime
			markAsRed(msgID);	
		}

		function deleteMessage(){
			if(confirm("Warning! \n You are about to delete all selected messages")){
				var checkBoxes = document.getElementsByName('checkBox');
				var deleteID = [];
				for(i = 0; i<checkBoxes.length; i++)
					if (checkBoxes[i].checked) 
						deleteID.push(checkBoxes[i].id);

				if (window.XMLHttpRequest){
				// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlhttp=new XMLHttpRequest();
				}else{ 
				// code for IE6, IE5
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlhttp.onreadystatechange = function() {
					if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
						//if successfull do something
						document.getElementById("sendMsgResult").innerHTML = xmlhttp.responseText;
						location.reload();
					}
				}
		       	xmlhttp.open("POST","func_del_msgs.php",true);
		       	xmlhttp.setRequestHeader("Content-type", "application/json" );
		        xmlhttp.send(JSON.stringify(deleteID));
				}
		}

		//Highlights the selected row
		function toggleBckGnd(Chkbox){
			if(Chkbox.checked){
				Chkbox.parentNode.parentNode.style.background = '#F0F0F0';
			} else {
				Chkbox.parentNode.parentNode.style.background = '';
			}
		}
		//Hides everything and shows the compose table
		function showNewMessage(msgSubject,sender,group){
			document.getElementById("divInbox").style.display = "none";
			document.getElementById("tableDisplayMsg").style.display = "none";
			document.getElementById("tableNewMsg").style.display = "inline";
			sendButton = document.getElementById("sendMsg");
					
			if(sender){
				document.getElementById("rowSubjectNew").innerHTML = "Re: " + msgSubject.innerHTML;
				document.getElementById("rowRecipNew").innerHTML = sender.innerHTML.substring(sender.innerHTML.indexOf(':')+2, sender.innerHTML.length);;
			}
			if(group){
				sendButton.onclick = sendGroupMsg;
				sendButton.innerHTML = "Send Group Message";
			} else {
				sendButton.onclick = saveSwpMessage;
				sendButton.innerHTML = "Send Message";
			}
		}

		//Hides everything and shows the Message Table
		function showInbox(){
			document.getElementById("tableDisplayMsg").style.display = "none";
			document.getElementById("tableNewMsg").style.display = "none";
			document.getElementById("divInbox").style.display = "inline";
			document.getElementById("sendMsgResult").innerHTML = "";
		}

		function saveSwpMessage() {
			if (window.XMLHttpRequest){
			// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}else{ 
			// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					//if successfull do something
					result = document.getElementById("sendMsgResult");
					result.innerHTML = xmlhttp.responseText;
					if(result.innerHTML != "<h1>There was a problem sending the message</h1>"){
						document.getElementById("divInbox").style.display = "inline";
						document.getElementById("tableNewMsg").style.display = "none";						
					}					
					location.reload();
				}
			}
			rowSubject = document.getElementById("rowSubjectNew").innerHTML;
			rowBody = document.getElementById("rowBodyNew").innerHTML;
			rowRecip = document.getElementById("rowRecipNew").innerHTML;

			rowSubject.replace(/<(?:.|\n)*?>/gm, '');
			rowBody.replace(/<(?:.|\n)*?>/gm, '');
			rowRecip.replace(/<(?:.|\n)*?>/gm, '');

	       	xmlhttp.open("POST","save_message.php",true);
	       	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	        xmlhttp.send( 'Recip='+ rowRecip +'&Subject=' + rowSubject + '&Body=' + rowBody );
	        //if owner of group, send a message to each member, otherwise send message to owners of group
	      }

	      function sendGroupMsg(){
	      				if (window.XMLHttpRequest){
			// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			}else{ 
			// code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
					//if successfull do something
					result = document.getElementById("sendMsgResult");
					result.innerHTML = xmlhttp.responseText;
					if(result.innerHTML != "<h1>There was a problem sending the message</h1>"){
						document.getElementById("divInbox").style.display = "inline";
						document.getElementById("tableNewMsg").style.display = "none";
					}					
				}
			}
			rowSubject = document.getElementById("rowSubjectNew").innerHTML;
			rowBody = document.getElementById("rowBodyNew").innerHTML;
			rowRecip = document.getElementById("rowRecipNew").innerHTML;

	       	xmlhttp.open("POST","func_sendGroupMessage.php",true);
	       	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	        xmlhttp.send( 'Recip='+ rowRecip +'&Subject=' + rowSubject + '&Body=' + rowBody );
	      }


		function markAsRed(msgID){
			if (window.XMLHttpRequest){
			// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			}else{ 
			// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.open("POST","func_setDateOpened.php",true);
	       	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	        xmlhttp.send("msgID="+msgID);

		}

	</script>		
	</head>
	<body onload='loadUserMessages()'>
		<div id="header">
			<?php
			echo showBanner($uName, $allUTypes, $uTypeCode, "Messages");
			?>
		</div>
		<div id="leftnavigation">
			<div class='cssmenu'>
				<?php
				echo showMenu($uTypeCode);					
				?>
			</div>
		</div>
		<div id="content" name="content">
			<h1>Messaging</h1>	
			</br>
			<div id="divInbox">				
				<table style="width:100%; display:inline;" border="0" id="tableInbox" cellpadding="0" cellspacing="0">
					<tr style="background-color:#D0D0D0">
						<td style="width:10%">Delete</td>
						<td style="width:50%">Subject</td>		
						<td style="width:20%">From</td>
						<td style="width:20%">Date</td>
					</tr>
				</table>
				</br>
				<table style="width:100%; display:inline;" border="0" cellpadding="0" cellspacing="0">
					<tr id='buttons'>
						<td style="width:100px" onclick='showNewMessage()'>New User Message</td>
						<td style="width:100px" onclick='showNewMessage("","",true)'>New Group Message</td>
						<td style="width:100px" onclick='deleteMessage()'>Delete Selected Messages</td>
					</tr>
				</table>
			</div>
			<div>
				<table style="display:none; width:100%" id="tableDisplayMsg"border="0" cellpadding="0" cellspacing="0" >
					<tr id='buttons'>
						<td style="width:20%" onclick='showNewMessage(rowSubject, rowSender)'>Reply</td>
						<td style="width:20%" onclick='deleteMessage()'>Delete</td>
						<td style="width:20%" onclick='showInbox()'>Back</td>
					</tr> 
					<tr>
						<td colspan="2" id="rowSubject" name="rowSubject" style="padding:10px; border-bottom: 1px solid #A0A0A0;">.</td>
						<td colspan="2" id="rowSender" name="rowSender" style="padding:10px; border-bottom: 1px solid #A0A0A0;border-left: 1px solid #A0A0A0;">.</td>
					</tr>
					<tr>
						<td colspan="4" id="rowBody" style="padding:10px; min-height:200px; max-width: inherit; inherit;overflow-wrap: break-word;">.</td>
					</tr>
				</table>
			</div>
			<div>
				<table style="display:none; width:100%" onload id="tableNewMsg" border="0" cellpadding="0" cellspacing="0" >
					<tr id='buttons' style="background-color:#D0D0D0" >
						<td style="width:60%" id='sendMsg' onclick='saveSwpMessage()'>Send Message</td>
						<td style="width:20%" onclick='clearAlert()'>Clear</td>
						<td style="width:20%" onclick='showInbox()'>Cancel</td>
					</tr> 
					<tr>
						<td id="rowSubjectNew" data-ph="Subject" contentEditable=true></td>
						<td colspan='2' id="rowRecipNew" data-ph="To" contentEditable=true></td>
					</tr>
					<tr>
						<td colspan="4" id="rowBodyNew" data-ph="Body" style="padding:10px; height:200px; vertical-align: text-top;" contentEditable=true></td>
					</tr> 
				</table>
				<p id='sendMsgResult'>					
				</p>
			</div>
	<div id="footer">
		<h2>Bottom</h2>
		footer.
	</div>
</body>
</html>
