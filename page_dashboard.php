<?php

include("session.php");
include("misc_functions.php");
include("db_access_details.php");

?>

<HTML>
	<head>
		<title>Dashboard</title>
		<link href="style.css" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</style>
	<script type="text/javascript">
	function showAlert() {
		alert("!");
	}
	</script>		
</head>
<body>
	<div id="header">
		<?php
		echo showBanner($uName, $allUTypes, $uTypeCode, "User Dashboard");
		?>
	</div>
	<div id="leftnavigation">
		<div class='cssmenu'>
			<?php
			echo showMenu($uTypeCode);
			?>
		</div>
	</div>
	<div id="content" name="content">
		
		<?php
			$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
			$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$sql = "SELECT COUNT(*) FROM `message` WHERE msg_recip_id = '$loggedInUserID' AND  msg_date_opened IS NULL";
			$qry = $conn -> prepare($sql);
			$qry -> execute();
			$rows = $qry->fetch();
			echo "<h1><a href='page_messaging.php'> You have ".$rows['COUNT(*)']." Unread Messages</a></h1>";

			$sql = "SELECT evt_name, evt_datetime,evt_desc, usr_username , grp_name
			FROM event
			INNER JOIN users
			ON evt_creator_id = usr_id			
			LEFT JOIN groups
			ON event.grp_id = groups.grp_ID";
			$qry = $conn -> prepare($sql);
			$qry -> execute();

			$announcements = "";
			$groupEvents = "";			
			$currentDate = new DateTime();

			foreach ($qry as $row){
				$eventDate = new DateTime($row['evt_datetime']);
				if ($eventDate > $currentDate){
					if($row['grp_name'] != NULL){
						$groupEvents.= "<p id='card'><b>".$row['evt_name']."</b> by ".$row['usr_username']." <b>Group Name: </b>". $row['grp_name'];
						$groupEvents.= "<br>" . date_format(date_create(strval($row['evt_datetime'])),'g:ia \o\n l jS F Y');
						$groupEvents.= "<br>" .$row['evt_desc'];
						$groupEvents.= "</p>";
					} else {
						$announcements.= "<p id='card'><b>".$row['evt_name']."</b> by ".$row['usr_username'];
						$announcements.= "<br>" . date_format(date_create(strval($row['evt_datetime'])),'g:ia \o\n l jS F Y');
						$announcements.= "<br>" .$row['evt_desc'];
						$announcements.= "</p>";
					}
				}
			}
			if($announcements != "")
				echo "<h1>Public Announcements</h1>" . $announcements;
			else
				echo "<h1>No Public Announcements</h1>";

			if($groupEvents != "")
				echo "<h1>Group Events</h1>" . $groupEvents;
			else
				echo "<h1>No Group Events</h1>"; 
		?>
	</div>
</body>
</html>
