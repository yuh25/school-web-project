<?php
include("db_access_details.php");
include("session.php");
include('misc_functions.php');
include('action_logging.php');

$data = file_get_contents( "php://input" );

$data = json_decode( $data );
$data  = implode(",", $data );

try {			
	$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	$sql = "DELETE FROM message WHERE msg_id IN ($data)";
	$qry = $conn -> prepare($sql);
	$qry -> execute();
	echo "<h1>Selected messages were Deleted.</h1>";	
	
} catch(PDOException $e) {
	Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
	"<h1>Selected messages were Deleted.</h1>";
}
?>

