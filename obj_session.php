<?php
	/*
	 * Desc: Class holding details for the web session.
	 * 
	 * */
	class SWPSession {
		
		private $usrUName;			// User Name (up to xxx characters)
		private $usrPWord;			// User Password (up to xxx characters)
		private $usrID;				// User ID, an integer
		private $usrTypeAdmin;		// User Type is Admin, a boolean
		private $usrTypeTeacher;	// User Type is Teacher, a boolean
		private $usrTypeGuardian;	// User Type is Guardian, a boolean
		private $usrTypeStudent;	// User Type is Student, a boolean (shouldn't also be any of the above.)
		private $usrFName;			// User First Name (up to xxx characters)
		private $usrLName;			// User Last Name (up to xxx characters)
		private $usrEmail;			// User Email Address (up to xxx characters)
		private $usrStudentYrLevel;	// User Student's Year Level, as an integer
		private $usrActive;			// User Active status, as a boolean
		 
		public function __construct(
			$uName, $uPword, $uID, $uTypeAdmin, $uTypeTeacher, $uTypeGuardian, $uTypeStudent, $uFName, $uLName,
			$uEmail, $uStudentYrLevel, $uActive
		) {
			$this -> usrUName = $uName;
			$this -> usrPWord = $uPword;
			$this -> usrID = $uID;
			$this -> usrTypeAdmin = $uTypeAdmin;
			$this -> usrTypeTeacher = $uTypeTeacher;
			$this -> usrTypeGuardian = $uTypeGuardian;
			$this -> usrTypeStudent = $uTypeStudent;
			$this -> usrFName = $uFName;
			$this -> usrLName = $uLName;
			$this -> usrEmail = $uEmail;
			$this -> usrStudentYrLevel = $uStudentYrLevel;
			$this -> usrActive = $uActive;			
		}
		
		public function getUsrUName() {
			return $this -> usrUName;
		}
		
		public function getUsrPWord() {
			return $this -> usrPWord;
		}
		
		public function getUsrID() {
			return $this -> usrID;
		}
		
		public function getUsrTypeAdmin() {
			return $this -> usrTypeAdmin;
		}
		
		public function getUsrTypeTeacher() {
			return $this -> usrTypeTeacher;
		}
		
		public function getUsrTypeGuardian() {
			return $this -> usrTypeGuardian;
		}
		
		public function getUsrTypeStudent() {
			return $this -> usrTypeStudent;
		}
		
		public function getUsrFName() {
			return $this -> usrFName;
		}
		
		public function getUsrLName() {
			return $this -> usrLName;
		}
		
		public function getUsrEmail() {
			return $this -> usrEmail;
		}
		
		public function getUsrStudentYrLevel() {
			return $this -> usrStudentYrLevel;
		}

		public function getUsrActive() {
			return $this -> usrActive;
		}
		
		public function getSwpUserTypeCode() {
			// Function to consolidate the user type(s):
			//
			// ASSUMPTION:if the user is a Student, they CANNOT be any other type.
			//
			// TYPE CODE:
			//      student     admin     teacher     guardian
			//         8          4          2            1
			// VALUES:
			//    8 : student
			//    7 : 4 + 2 + 1 == admin + teacher + guardian
			//    6 : 4 + 2 == admin + teacher
			//    5 : 4 + 1 == admin + guardian
			//    4 : admin
			//    3 : 2 + 1 == teacher + guardian
			//    2 : teacher
			//    1 : guardian
			//    0 OR >8 : ERROR!
			
			$retUserTypeCode = 0;
			
			if($this -> getUsrTypeStudent()){
				$retUserTypeCode = 8;
			}
			if($this -> getUsrTypeAdmin()) {
				$retUserTypeCode = $retUserTypeCode + 4;
			}
			if($this -> getUsrTypeTeacher()) {
				$retUserTypeCode = $retUserTypeCode + 2;
			}
			if($this -> getUsrTypeGuardian()) {
				$retUserTypeCode = $retUserTypeCode + 1;
			}
			return $retUserTypeCode;
		}

		public function getSwpUserTypeDesc() {
			
			$retUserTypeDesc = "";
			
			switch ($this -> getSwpUserTypeCode()) {
				case 0:
					$retUserTypeDesc = "unassigned";
					break;
				case 1:
					$retUserTypeDesc = "guardian";
					break;
				case 2:
					$retUserTypeDesc ="teacher";
					break;
				case 3:
					$retUserTypeDesc = "teacher guardian";
					break;
				case 4:
					$retUserTypeDesc = "admin";
					break;
				case 5:
					$retUserTypeDesc = "admin guardian";
					break;
				case 6:
					$retUserTypeDesc = "admin teacher";
					break;
				case 7:
					$retUserTypeDesc = "admin teacher guardian";
					break;
				case 8:
					$retUserTypeDesc = "student";
					break; 
				default:
					$retUserTypeDesc = "bad_user_type";
			}
			
			return $retUserTypeDesc;
		}
		
		// Check if the type description is some kind of admin:		
		public function isSwpAdmin() {
			
			if (strpos($this -> getSwpUserTypeDesc(), "admin") !== false) {
				return true;
			}
			
			return false;
		}
		
		// Check if the type description is some kind of student:
		public function isSwpTeacher() {
			
			if (strpos($this -> getSwpUserTypeDesc(), "teacher") !== false) {
				return true;
			}
			
			return false;
		}
		
		// Check if the type description is some kind of guardian:
		public function isSwpGuardian() {
			
			if (strpos($this -> getSwpUserTypeDesc(), "guardian") !== false) {
				return true;
			}
			
			return false;
		}
		
		// Check if the type description is some kind of student:
		public function isSwpStudent() {
			
			if (strpos($this -> getSwpUserTypeDesc(), "student") !== false) {
				return true;
			}
			
			return false;
		}	
	}