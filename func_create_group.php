<?php
    function createGroup($newGroupName,$newGroupOwner1,$newGroupOwner2,$newGroupOwner3,$loggedInUserID) {			

		// forget it for now: check out:
		// http://code.tutsplus.com/tutorials/php-database-access-are-you-doing-it-correctly--net-25338
		$stmt = "";
		$success = "";
		
		include("db_access_details.php");
		if(strlen($newGroupOwner1)){
			try {				
				$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
				$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);				

				//Converts usernames into user ID's
				$stmt = "select usr_id from users where usr_username in ('$newGroupOwner1', '$newGroupOwner2', '$newGroupOwner3')";
				$temp = $conn -> query($stmt);
				//Get the ID's from the result
				$row = $temp -> fetch();
				$user_id1 = $row["usr_id"];
				$row = $temp -> fetch();
				$user_id2 = $row["usr_id"];
				$row = $temp -> fetch();
				$user_id3 = $row["usr_id"];

				$val = "";

				if(strlen($user_id1))
					$val .= "'$user_id1',";	
				else
					$val .= "NULL,";	

				if(strlen($user_id2))
					$val .= "'$user_id2',";	
				else
					$val .= "NULL,";

				if(strlen($user_id3))
					$val .= "'$user_id3',";
				else
					$val .= "NULL,";	

				$stmt = "INSERT INTO `groups`( `grp_name`, `grp_owner1`, `grp_owner2`, `grp_owner3`, `grp_creator`, `grp_active`) VALUES ('$newGroupName', " . $val . " '$loggedInUserID', '1')";

				// Insert the new group:
				$conn -> exec($stmt);				

				$success = "<h1>Group created successfully</h1>";
			}
			catch(PDOException $e) {
				//echo 'ERROR: ' . $e -> getMessage();
				$success = "<h1>Group was not created</h1>";
			}			
		}
		else{			
			$success = "<h1>Invalid Username used</h1>";
		}
		$conn = null;
		return $success;
	}
?>