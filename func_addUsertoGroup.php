<?php
include("db_access_details.php");
include("session.php");
include('misc_functions.php');
include('action_logging.php');

$username = $_POST['username'];
$groupName = $_POST['groupName'];

try {			
	$conn = new PDO("mysql:host=$DBAx_dbhost;dbname=$DBAx_dbname;charset=utf8", $DBAx_dbuname, $DBAx_dbpword);
	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$conn -> setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	$sql = "SELECT usr_id from users where usr_username = '$username'";
	$qry = $conn -> prepare($sql);
	$qry -> execute();
	$rows = $qry->fetch();

	$sql = "SELECT usrgrp_active from users_groups where usr_id = '$rows[0]' AND grp_ID = '$groupName'";
	$qry = $conn -> prepare($sql);
	$qry -> execute();	

	if($rows3 = $qry->fetch()) {
		if(intval($rows3[0]) == 0){
			$sql = "UPDATE users_groups SET usrgrp_active= 1 WHERE grp_id='$groupName' AND usr_id='$rows[0]'";
			$qry = $conn -> prepare($sql);
			$qry -> execute();
		}
	} else {
		$sql= "INSERT INTO `users_groups`(`grp_id`, `usr_id`, `usrgrp_active`) VALUES ('$groupName','$rows[0]','1')";
		$qry = $conn -> prepare($sql);
		$qry -> execute();
	}
	echo "<h1>User was added to the group</h1>";
} catch(PDOException $e) {
	Errorlogthis( $e->getMessage().' in '.$e->getFile().' on line '.$e->getLine());
	echo "<h1>User was NOT added to the group</h1>";
}
?>