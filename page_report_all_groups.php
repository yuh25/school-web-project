<?php

	/*
	 * Generates a page for the Users Group report interface.
	 */

	include("session.php");
	include("misc_functions.php");
	include('func_make_report.php');

	
	if(isset($_POST['makeReportGroups'])) {

		//echo 'Go ahead + make the report, zip it up + offer to download it.';
		$groupToReportOn = isset($_POST['cboGroupsToReportOn']) ? $_POST['cboGroupsToReportOn'] : '';

		if(strval($groupToReportOn) !== '') {
			$rptMade = createSWPGroupReport(trim(strval($groupToReportOn)), $uName);
		}
	}

?>
	<HTML>
		<head>
			<title>Generate Report - Groups</title>
			<link href="style.css" rel="stylesheet" type="text/css" />
			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		</style>
			<script language="javascript" type="text/javascript">

			function showAlert() {
				alert("Function not available!");
			}
			</script>		
		</head>
	<body>
		<div id="header">
			<?php
			echo showBanner($uName, $allUTypes, $uTypeCode, "Reports");
			?>
		</div>
		<div id="leftnavigation">
			<div class='cssmenu'>
				<?php
				echo showMenu($uTypeCode);
				?>
			</div>
		</div>
		<div id="content" name="content">
			<h1>Generate Report - User Groups</h1>
		</br></br>
		<p>
			<form id="frmGroupsReport" name="frmGroupsReport" action = "" method="post">
			<!--
				Shows a combo/dropdown list of the user groups to report for.
			-->
			<p>
				Select group to report on: &nbsp;

				<select id="cboGroupsToReportOn" name="cboGroupsToReportOn" class="">
					<option value="all">All Groups</option>
					<?php
					echo getListOfGroups();	// Found in misc_functions.php
					?>
				</select>
			</p>
			<p>
				Click to export report & download: &nbsp; 
				<input type="reset" name="makeReportGroups" id="makeReportGroups" value="Export" onclick='showAlert()'>
			</p>
		</form>
	</p>
</div>
<div id="footer">
	<h2>Bottom</h2>
	footer.
</div>
</body>
</html>