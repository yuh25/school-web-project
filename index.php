<?php

	if(isset($_POST['credentialLogin'])) {
		include('login.php');
	}
	

	?>
<!DOCTYPE html>

	<html>
	<head>
		<title>School Web Portal - Log In</title>
		<link href="style_login.css" rel="stylesheet" type="text/css" />
	</style>
</head>
<body>
<div class="wrapper">
	<div class="main">
		<div class="title">
			<h1>School Web Portal</h1>
				<div class="content">
					<p>
			Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.
					</p>
	</div>
		</div>
		
		<div class="login">
			<h1>Log In</h1>
				<form class="loginForm" name="credentialEntry" action = "" method="post">

						<div class="field">
							<input type="text" name="credentialUsername" required />
							<span class="line"></span>
							<label>Username</label>
						</div>
						<div class="field">
							<input type="password" name="credentialPassword" required/>
							<span class="line"></span>
							<label>Password</label>
						</div>

					<div>
						<input type="submit" name="credentialLogin" value="Log In" />
						<a class="termslink" onClick = "window.open('terms_conditions.html', '_blank', 'toolbar=yes, scrollbars=yes, resizable=yes, top=100, left=100, width=450, height=600');" href="#">
							Terms & Conditions</a>
					</div>
				</form>
						<div id="loginFailed">
							<?php
							if(isset($loginFailed))
							echo $loginFailed;
							?>
						</div>
		</div>
	</div>
</div>
</body>
</html>
